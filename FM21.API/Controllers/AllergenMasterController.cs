﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;

namespace FM21.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("All Allergern Master related API methods")]
    //[ApiExplorerSettings(GroupName = "v1.0")]
    [ApiController]
    public class AllergenMasterController : ControllerBase
    {
        private readonly IStringLocalizer localizer;
        private readonly IAllergenMasterService allergenMasterService;

        public AllergenMasterController(IStringLocalizer localizer, IAllergenMasterService allergenMasterService)
        {
            this.localizer = localizer;
            this.allergenMasterService = allergenMasterService;
        }

        [SwaggerOperation(Summary = "Get Allergen list for search (page wise, sort, filter).", Tags = new string[] { "AllergenMaster" })]
        [HttpGet("GetSearchList")]
        public async Task<IActionResult> GetSearchList([FromQuery] SearchFilter searchFilter)
        {
            if (ModelState.IsValid)
            {
                var response = new PagedResult<AllergenMaster>();
                response = await allergenMasterService.GetPageWiseData(searchFilter.Search, searchFilter.PageIndex, searchFilter.PageSize, searchFilter.SortColumn, searchFilter.SortDirection);
                return new OkObjectResult(response);
            }
            else
            {
                return BadRequest();
            }
        }

        [SwaggerOperation(Summary = "Get list of all Allergen List.", Tags = new string[] { "AllergenMaster" })]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = new GeneralResponse<ICollection<AllergenMaster>>();
            response = await allergenMasterService.GetAll();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get Allergen by AllergenId.", Tags = new string[] { "AllergenMaster" })]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAllergen(int id)
        {
            var obj = await allergenMasterService.Get(id);
            if (obj == null)
            {
                return NotFound(obj);
            }
            return new OkObjectResult(obj);
        }

        [SwaggerOperation(Summary = "Add new Allergen.", Tags = new string[] { "AllergenMaster" })]
        [HttpPost]
        public async Task<ActionResult<AllergenMaster>> PostAllergen(AllergenMasterModel model)
        {
            var response = await allergenMasterService.Create(model);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Update existing Allergen.", Tags = new string[] { "AllergenMaster" })]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAllergen(int id, AllergenMasterModel model)
        {
            model.AllergenID = id;
            var response = await allergenMasterService.Update(model);
            return new JsonResult(response);
        }
        [SwaggerOperation(Summary = "Delete the Allergen.", Tags = new string[] { "AllergenMaster" })]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAllergen(int id)
        {
            var response = await allergenMasterService.Delete(id);
            return new JsonResult(response);
        }
    }
}
