﻿using AutoMapper;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("All the common API methods")]
    //[ApiExplorerSettings(GroupName = "v1.0")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly IStringLocalizer localizer;
        private readonly ICommonService commonService;

        public CommonController(IStringLocalizer localizer, ICommonService commonService)
        {
            this.localizer = localizer;
            this.commonService = commonService;
        }

        [SwaggerOperation(Summary = "Get list of all the sites.", Tags = new string[] { "SiteMaster" })]
        [HttpGet("GetAllSite")]
        public async Task<IActionResult> GetAllSite()
        {
            var response = new GeneralResponse<ICollection<SiteMaster>>();
            response = await commonService.GetAllSite();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get list of all the Instruction category.", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("GetAllInstructionCategory")]
        public async Task<IActionResult> GetAllInstructionCategory()
        {
            var response = new GeneralResponse<ICollection<InstructionCategoryMaster>>();
            response = await commonService.GetAllInstructionCategory();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get list of Instruction category by site.", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("GetAllInstructionCategoryBySiteID")]
        public async Task<IActionResult> GetAllInstructionCategoryBySiteID(int siteID)
        {
            var response = new GeneralResponse<ICollection<InstructionCategoryMaster>>();
            response = await commonService.GetAllInstructionCategoryBySiteID(siteID);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get list of all the Instruction group.", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("GetAllInstructionGroup")]
        public async Task<IActionResult> GetAllInstructionGroup()
        {
            var response = new GeneralResponse<ICollection<InstructionGroupMaster>>();
            response = await commonService.GetAllInstructionGroup();
            return new JsonResult(response);
        }
    }
}