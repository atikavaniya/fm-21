﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("All Instruction master related API methods")]
    //[ApiExplorerSettings(GroupName = "v1.0")]
    [ApiController]
    public class InstructionMasterController : ControllerBase
    {
        private readonly IStringLocalizer localizer;
        private readonly IInstructionMasterService instructionService;

        public InstructionMasterController(IStringLocalizer localizer, IInstructionMasterService instructionService)
        {
            this.localizer = localizer;
            this.instructionService = instructionService;
        }

        [SwaggerOperation(Summary = "Get list of all Instructions.", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = new GeneralResponse<ICollection<InstructionMasterModel>>();
            response = await instructionService.GetAll();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get Instruction list for search (page wise, sort, filter).", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("GetSearchList")]
        public async Task<IActionResult> GetSearchList([FromQuery] SearchFilter searchFilter)
        {
            if (ModelState.IsValid)
            {
                var response = new PagedResult<InstructionMasterModel>();
                response = await instructionService.GetPageWiseData(searchFilter);
                return new JsonResult(response);
            }
            else
            {
                return BadRequest();
            }
        }

        [SwaggerOperation(Summary = "Get Instruction by InstructionMasterID.", Tags = new string[] { "InstructionMaster" })]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInstruction(int id)
        {
            var obj = await instructionService.Get(id);
            if (obj == null)
            {
                return NotFound();
            }
            return new JsonResult(obj);
        }

        [SwaggerOperation(Summary = "Add new Instruction.", Tags = new string[] { "InstructionMaster" })]
        [HttpPost]
        public async Task<ActionResult<InstructionMaster>> PostInstruction(InstructionMasterModel model)
        {
            var response = await instructionService.Create(model);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Update existing Instruction.", Tags = new string[] { "InstructionMaster" })]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInstruction(int id, InstructionMasterModel model)
        {
            model.InstructionMasterID = id;
            var response = await instructionService.Update(model);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Delete the Instruction.", Tags = new string[] { "InstructionMaster" })]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInstruction(int id)
        {
            var response = await instructionService.Delete(id);
            return new JsonResult(response);
        }
    }
}