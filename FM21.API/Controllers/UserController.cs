﻿using AutoMapper;
using FluentValidation;
using FM21.API.Helpers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Core.Validator;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FM21.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("All User and permission related API methods")]
    //[ApiExplorerSettings(GroupName = "v1.0")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IWebWorker webWorker;
        private readonly IStringLocalizer localizer;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly ICustomerService customerService;

        public UserController(IWebWorker webWorker, IStringLocalizer localizer,
            IUserService userService,
            IMapper mapper,
            ICustomerService customerService)
        {
            this.webWorker = webWorker;
            this.localizer = localizer;
            this.mapper = mapper;
            this.userService = userService;
            this.customerService = customerService;
        }

        [SwaggerOperation(Summary = "Check user is authorized with respect to AD", Tags = new string[] { "User" })]
        [HttpGet("ValidateUser")]
        public IActionResult ValidateUser()
        {
            var response = new GeneralResponse<string>();
            if (User.Identity.IsAuthenticated)
            {
                response.Result = ResultType.Success;
                response.Data = User.Identity.Name;
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            }
            else
            {
                response.Result = ResultType.Error;
                response.Message = "You are not authorized";
            }
            return Ok(response);
        }

        [SwaggerOperation(Summary = "Get user name", Tags = new string[] { "User" })]
        [HttpGet("GetUserName")]
        [Authorization(PermissionItem.User, PermissionAction.Read)]
        public IActionResult GetUserName()
        {
            var response = new GeneralResponse<string>();
            response.Result = ResultType.Success;
            response.Data = User.Identity.Name;
            return Ok(response);
        }

      
    }
}