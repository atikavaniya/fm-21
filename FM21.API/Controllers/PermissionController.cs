﻿using AutoMapper;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace FM21.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [SwaggerTag("All Permission master related API methods")]
    //[ApiExplorerSettings(GroupName = "v1.0")]
    [ApiController]
   
    public class PermissionController : ControllerBase
    {
        private readonly IStringLocalizer localizer;
        private readonly IPermissionMasterService permissionMasterService;

        public PermissionController(IStringLocalizer localizer, IPermissionMasterService permissionMasterService)
        {
            this.localizer = localizer;
            this.permissionMasterService = permissionMasterService;
        }

        [SwaggerOperation(Summary = "Get list of all Permission.", Tags = new string[] { "Permission" })]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = new GeneralResponse<ICollection<PermissionMaster>>();
            response = await permissionMasterService.GetAll();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get Permission list for search (page wise, sort, filter).", Tags = new string[] { "Permission" })]
        [HttpGet("GetSearchList")]
        public async Task<IActionResult> GetSearchList([FromQuery] SearchFilter searchFilter)
        {
            if (ModelState.IsValid)
            {
                var response = new PagedResult<PermissionMaster>();
                response = await permissionMasterService.GetPageWiseData(searchFilter.Search, searchFilter.PageIndex, searchFilter.PageSize, searchFilter.SortColumn, searchFilter.SortDirection);
                return new JsonResult(response);
            }
            else
            {
                return BadRequest();
            }
        }

        [SwaggerOperation(Summary = "Get Permision by PermissionId.", Tags = new string[] { "Permission" })]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPermission(int id)
        {
            var obj = await permissionMasterService.Get(id);
            if (obj == null)
            {
                return NotFound();
            }
            return new JsonResult(obj);
        }

        [SwaggerOperation(Summary = "Add new Permission.", Tags = new string[] { "Permission" })]
        [HttpPost]
        public async Task<ActionResult<PermissionMaster>> PostPermission(PermissionMasterModel model)
        {
            var response = await permissionMasterService.Create(model);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Update existing Permission.", Tags = new string[] { "Permission" })]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPermission(int id, PermissionMasterModel model)
        {
            model.PermissionID = id;
            var response = await permissionMasterService.Update(model);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Delete the Permission.", Tags = new string[] { "Permission" })]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePermission(int id)
        {
            var response = await permissionMasterService.Delete(id);
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Get Role Permission matrix.", Tags = new string[] { "Permission" })]
        [HttpGet("GetRolePermissionMatrix")]
        public async Task<IActionResult> GetRolePermissionMatrix()
        {
            var response = new GeneralResponse<ICollection<RolePermissionMatrix>>();
            response = await permissionMasterService.GetRolePermissionMatrix();
            return new JsonResult(response);
        }

        [SwaggerOperation(Summary = "Add new Permission.", Tags = new string[] { "Permission" })]
        [HttpPost("PostRolePermissionMatrix")]
        public async Task<ActionResult<List<RolePermissionAccess>>> PostRolePermissionMatrix([FromBody] List<RolePermissionAccess> roleAccessList)
        {
             var response = await permissionMasterService.UpdateRoleMatrix(roleAccessList);
            //List<RolePermissionAccess> obj = new List<RolePermissionAccess>();
            return new JsonResult(response);
        }
    }
}
