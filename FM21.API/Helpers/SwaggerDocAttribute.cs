﻿using System;

namespace FM21.API.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class SwaggerDocAttribute : Attribute
    {
        public SwaggerDocAttribute(params string[] includeInDocuments)
        {
            IncludeInDocuments = includeInDocuments;
        }

        public string[] IncludeInDocuments { get; }
    }
}