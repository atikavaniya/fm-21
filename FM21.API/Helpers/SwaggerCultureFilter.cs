﻿using FM21.Core;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace FM21.API.Helpers
{
    public class SwaggerCultureFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "culture",
                In = ParameterLocation.Header,
                AllowEmptyValue = false,
                Required = true,
                Description = ConfigurationConstants.SupportedLocalization
            });
        }
    }
}