﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Reflection;

namespace FM21.API.Helpers
{
    public class SwaggerExcludeFilters : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext schemaRegistry)
        {
            if (schema?.Properties == null || schema.Type == null)
                return;

            var excludedProperties = schemaRegistry.Type.GetProperties().Where(t => t.GetCustomAttribute<SwaggerExcludeAttribute>() != null);
            foreach (var excludedProperty in excludedProperties)
            {
                if (schema.Properties.ContainsKey(excludedProperty.Name))
                    schema.Properties.Remove(excludedProperty.Name);
            }

            var excludedProperties1 = schemaRegistry.Type.GetProperties().Where(t => t.GetCustomAttribute<Newtonsoft.Json.JsonIgnoreAttribute>() != null);
            foreach (var excludedProperty in excludedProperties1)
            {
                if (schema.Properties.Any(p => p.Key.ToLower() == excludedProperty.Name.ToLower()))
                {
                    schema.Properties.Remove(excludedProperty.Name.ToLower());
                }
            }

            var excludedProperties2 = schemaRegistry.Type.GetProperties().Where(t => t.GetCustomAttribute<System.Text.Json.Serialization.JsonIgnoreAttribute>() != null);
            foreach (var excludedProperty in excludedProperties2)
            {
                if (schema.Properties.Any(p => p.Key.ToLower() == excludedProperty.Name.ToLower()))
                {
                    schema.Properties.Remove(excludedProperty.Name.ToLower());
                }
            }
        }
    }
}