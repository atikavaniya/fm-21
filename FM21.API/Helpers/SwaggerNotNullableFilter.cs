﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace FM21.API.Helpers
{
    public class SwaggerNotNullableFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext schemaRegistry)
        {
            if (schema?.Properties == null || schema.Type == null)
                return;
            var excludedProperties = schemaRegistry.Type.GetProperties().Where(t => t.GetCustomAttribute<RequiredAttribute>() != null);
            foreach (var excludedProperty in excludedProperties)
            {
                if (schema.Properties.ContainsKey(excludedProperty.Name))
                    schema.Properties[excludedProperty.Name].Nullable = false;
            }
        }
    }
}