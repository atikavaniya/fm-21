﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FM21.API.Helpers
{
    public enum PermissionItem
    {
        User,
        Product,
        Contact,
        Review,
        Client
    }

    public enum PermissionAction
    {
        Read,
        Modify,
    }

    public class AuthorizationAttribute : TypeFilterAttribute
    {
        public AuthorizationAttribute(PermissionItem item, PermissionAction action)
        : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item, action };
        }
    }

    public class AuthorizeActionFilter : IAuthorizationFilter
    {
        private readonly PermissionItem _item;
        private readonly PermissionAction _action;
        public AuthorizeActionFilter(PermissionItem item, PermissionAction action)
        {
            _item = item;
            _action = action;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            bool isAuthorized = context.HttpContext.User.Identity.IsAuthenticated;

            if (!isAuthorized)
            {
                context.Result = new ForbidResult();
            }
        }
    }
}