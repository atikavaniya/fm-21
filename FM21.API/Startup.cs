using AutoMapper;
using FM21.API.Extensions;
using FM21.API.Helpers;
using FM21.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace FM21.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            SetupAppConfigKeys();
            ServiceExtensions.ConfigureLoggerService(configuration);
        }

        /// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => 
                        {
                            options.Filters.Add(typeof(ValidateModelAttribute)); 
                        })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            services.AddMemoryCache();
            services.ConfigureCors();
            services.ConfigureIISIntegration();
            //services.ConfigureDBContext();
            services.ConfigureRepositoryAndServices();
            services.SetupAPIVersioning();
            services.ConfigureControllerAndJsonSettings();
            services.ConfigureLocalization();
            services.ConfigureSwagger();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
    }

        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IExceptionHandler exceptionHandler)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseGlobalExceptionHandler(exceptionHandler);
            app.UseApiVersioning();
            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();
            app.AddLocalization();
            app.AddSwagger(env);
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            exceptionHandler.LogInformation("API Started..");
        }

        /// <summary>
        /// Fectch and set global variables
        /// </summary>
        public void SetupAppConfigKeys()
        {
            ConfigurationConstants.DbConnectionString = Configuration.GetConnectionString("AppDBContextCon");
            ConfigurationConstants.EnableLog = Configuration.GetSection("AppSettings").GetValue<bool>("EnableLog");
            ConfigurationConstants.LogFilePath = Configuration.GetSection("AppSettings").GetValue<string>("LogFilePath");
            ConfigurationConstants.SupportedLocalization = Configuration.GetSection("AppSettings").GetValue<string>("Localization");
            ConfigurationConstants.CacheDurationInSecond = Configuration.GetSection("AppSettings").GetValue<int>("CacheDurationInSecond");
        }
    }
}