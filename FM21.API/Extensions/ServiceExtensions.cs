﻿using FluentValidation;
using FM21.API.Helpers;
using FM21.Core;
using FM21.Core.Localization;
using FM21.Core.Model;
using FM21.Core.Validator;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Data.Repository;
using FM21.Service;
using FM21.Service.Caching;
using FM21.Service.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using Serilog.Events;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FM21.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("CorsPolicy", policyBuilder =>
            {
                policyBuilder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = true;
            });
        }

        public static void SetupAPIVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(versioningSetupAction =>
            {
                versioningSetupAction.ReportApiVersions = true;
                versioningSetupAction.AssumeDefaultVersionWhenUnspecified = true;
                versioningSetupAction.DefaultApiVersion = new ApiVersion(1, 0);
                versioningSetupAction.UseApiBehavior = true;
            });
        }

        public static void ConfigureControllerAndJsonSettings(this IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });
        }

        public static void ConfigureDBContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<AppEntities>(options => options.UseSqlServer(ConfigurationConstants.DbConnectionString));
        }

        public static void ConfigureRepositoryAndServices(this IServiceCollection services)
        {
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDatabaseFactory, DatabaseFactory>();
            services.AddScoped<ICacheProvider, CacheProvider>();
            services.Add(new ServiceDescriptor(typeof(IRepository<>), typeof(Repository<>), ServiceLifetime.Scoped));
            services.AddScoped<IWebWorker, WebWorker>();
            services.AddSingleton<IExceptionHandler, ExceptionHandler>();
           
            #region Repository
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            #endregion

            #region Services
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRoleMasterService, RoleMasterService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IInstructionMasterService, InstructionMasterService>();
            services.AddScoped<IPermissionMasterService, PermissionMasterService>();
            services.AddScoped<ISupplierMasterService, SupplierMasterService>();
            services.AddScoped<IRegulatoryMasterService, RegulatoryMasterService>();
            services.AddScoped<IAllergenMasterService, AllergenMasterService>();
            #endregion

            #region Validator
            services.AddSingleton<IValidator<RoleMasterModel>, RoleMasterValidator>();
            services.AddSingleton<IValidator<PermissionMasterModel>, PermissionMasterValidator>();
            services.AddSingleton<IValidator<InstructionMasterModel>, InstructionMasterValidator>();       
            services.AddSingleton<IValidator<CustomerModel>, CustomerValidator>();
            
            services.AddSingleton<IValidator<RegulatoryModel>, RegulatoryMasterValidator>();

            services.AddSingleton<IValidator<SupplierMasterModel>, SupplierMasterValidator>();
            services.AddSingleton<IValidator<AllergenMasterModel>, AllergenMasterValidator>();
            #endregion
        }

        public static void ConfigureLoggerService(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
                            .Filter.ByExcluding(_ => !ConfigurationConstants.EnableLog)
                            .MinimumLevel.Debug()
                            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                            .Enrich.FromLogContext()
                            .WriteTo.File(
                                ConfigurationConstants.LogFilePath,
                                fileSizeLimitBytes: 1_000_000,
                                rollingInterval: RollingInterval.Day,
                                rollOnFileSizeLimit: true,
                                shared: true,
                                flushToDiskInterval: TimeSpan.FromSeconds(1)
                            )
                            .CreateLogger();
        }

        public static void ConfigureLocalization(this IServiceCollection services)
        {
            services.AddSingleton<IdentityLocalizationService>();
            services.AddLocalization(o =>
            {
                // We will put our translations in a folder called Resources
                o.ResourcesPath = "Resources";
            });
            services.AddSingleton<IStringLocalizerFactory, JsonStringLocalizerFactory>();
            services.AddSingleton<IStringLocalizer, JsonStringLocalizer>();
            services.AddMvc()
                    .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, opts => { opts.ResourcesPath = "Resources"; })
                    .AddDataAnnotationsLocalization(options => { });
            CultureInfo.CurrentCulture = new CultureInfo("en-US");
        }

        public static void AddLocalization(this IApplicationBuilder app)
        {
            IList<CultureInfo> supportedCultures = new List<CultureInfo>();
            foreach (var item in ConfigurationConstants.SupportedLocalization.Split(","))
            {
                supportedCultures.Add(new CultureInfo(item));
            }
            var localizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };
            app.UseRequestLocalization(localizationOptions);

            var requestProvider = new RouteDataRequestCultureProvider();
            localizationOptions.RequestCultureProviders.Insert(0, requestProvider);
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);
        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            Dictionary<string, string> SwaggerPolicies = new Dictionary<string, string> { };

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FM21 API", Version = "v1" });
                c.OperationFilter<SwaggerCultureFilter>();
                //c.SchemaFilter<SwaggerExcludeFilters>();
                //c.SchemaFilter<SwaggerNotNullableFilter>();
                //c.SchemaFilter<AutoRestSchemaFilter>();
                //c.DocumentFilter<SwaggerSecurityTrimming>();
                c.EnableAnnotations();
                c.DescribeAllEnumsAsStrings();
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert token with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                           Reference = new OpenApiReference{Type = ReferenceType.SecurityScheme,Id = "Bearer"}
                        },
                        SwaggerPolicies?.Keys.ToArray()
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                if (File.Exists(xmlPath))
                {
                    c.IncludeXmlComments(xmlPath);
                }
            });
        }

        public static void AddSwagger(this IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "/swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                //c.RoutePrefix = "api";
                c.DocumentTitle = $"App Api: {env?.EnvironmentName}";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "App Api v1");
                c.DefaultModelExpandDepth(2);
                //c.DefaultModelRendering(ModelRendering.Example);
                //c.DefaultModelsExpandDepth(-1);
                //c.DisplayRequestDuration();
                c.DocExpansion(DocExpansion.None);
                // c.EnableFilter();
                // c.ShowExtensions();
                c.EnableValidator();
            });
        }
    }
}