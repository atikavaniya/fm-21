﻿CREATE TABLE PermissionMaster
(
	PermissionID INT IDENTITY(1, 1) NOT NULL,
	PermissionFor VARCHAR(100) NOT NULL,
	IsDeleted bit NULL DEFAULT 0, 
	CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT PK_PermissionMaster PRIMARY KEY (PermissionID)
)