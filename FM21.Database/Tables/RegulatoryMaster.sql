﻿
CREATE TABLE [dbo].[RegulatoryMaster](
	[RegulatoryID] [int] IDENTITY(1,1) NOT NULL,
	[Nutrient] [varchar](50) NOT NULL,
	[OldUsa] [int] NULL,
	[CanadaNi] [int] NULL,
	[CanadaNf] [int] NULL,
	[NewUsRdi] [int] NULL,
	[Eu] [int] NULL,
	[Unit] [varchar](50) NOT NULL,
	[UnitPerMg] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegulatoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
