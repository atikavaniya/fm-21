﻿CREATE TABLE [dbo].[AllergenMaster]
(
	[AllergenID] INT IDENTITY(1,1) NOT NULL, 
    [AllergenCode] VARCHAR(10) NOT NULL, 
    [AllergenName] varchar(100) Not Null,
    [AllergenDescription_En] VARCHAR(500) NULL, 
    [AllergenDescription_Fr] NVARCHAR(500) NULL, 
    [AllergenDescription_Es] NVARCHAR(500) NULL, 
    [IsDeleted] BIT NULL DEFAULT 0, 
    [IsActive] BIT NULL DEFAULT 1,
    CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	UpdatedBy INT,
	UpdatedOn DATETIME DEFAULT GETDATE() NULL,
	CONSTRAINT PK_AllergenMaster PRIMARY KEY (AllergenID)
)
