﻿CREATE TABLE InstructionSiteMapping
(
	InstructionSiteMapID INT IDENTITY(1,1) NOT NULL,
	SiteID INT NOT NULL,
	InstructionCategoryID INT NOT NULL,
	CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL
	CONSTRAINT PK_InstructionSiteMapping PRIMARY KEY (InstructionSiteMapID),
	CONSTRAINT FK_InstructionSiteMapInstructionCategory FOREIGN KEY (InstructionCategoryID) REFERENCES InstructionCategoryMaster(InstructionCategoryID),
	CONSTRAINT FK_InstructionSiteMapSiteMaster FOREIGN KEY (SiteID) REFERENCES SiteMaster(SiteID)
)