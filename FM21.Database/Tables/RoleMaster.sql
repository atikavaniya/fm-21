﻿CREATE TABLE RoleMaster
(
    RoleID INT IDENTITY(1,1) NOT NULL,
    RoleName VARCHAR(50) NOT NULL,
	RoleDescription VARCHAR(100),
    IsActive bit  NULL DEFAULT 1,
	IsDeleted bit Null Default 0,
	CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	UpdatedBy INT,
	UpdatedOn DATETIME DEFAULT GETDATE(),
	CONSTRAINT PK_RoleMaster PRIMARY KEY (RoleID)
);