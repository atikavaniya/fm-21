﻿CREATE TABLE InstructionCategoryMaster
(
    InstructionCategoryID INT IDENTITY(1,1) NOT NULL,
    InstructionCategory NVARCHAR(100) NOT NULL,
    [Status] TINYINT NOT NULL DEFAULT 1,
    CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	CONSTRAINT PK_InstructionCategoryMaster PRIMARY KEY (InstructionCategoryID)
);