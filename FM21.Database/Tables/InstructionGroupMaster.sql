﻿CREATE TABLE InstructionGroupMaster
(
	InstructionGroupID INT IDENTITY(1,1) NOT NULL,
	InstructionGroupName NVARCHAR(100) NOT NULL,
	[Status] TINYINT DEFAULT(1) NOT NULL,
	CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	CONSTRAINT PK_InstructionGroupMaster PRIMARY KEY (InstructionGroupID)
)