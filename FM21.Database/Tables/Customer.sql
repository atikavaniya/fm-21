﻿CREATE TABLE Customer
(
    CustomerID INT IDENTITY(1,1) NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
	Email VARCHAR(50),
	[Address] NVARCHAR(200),
	PhoneNumber VARCHAR(20) NOT NULL,
	CustomerAbbreviation1 NVARCHAR(40),
	CustomerAbbreviation2 NVARCHAR(40),
    [IsActive] BIT DEFAULT(1) NULL,
	IsDeleted bit NULL DEFAULT 0,
	CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	UpdatedBy INT,
	UpdatedOn DATETIME DEFAULT GETDATE(),
	CONSTRAINT PK_Customer PRIMARY KEY (CustomerID)
);