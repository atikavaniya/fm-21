﻿CREATE TABLE InstructionMaster 
(
    InstructionMasterID INT IDENTITY(1,1) NOT NULL,
	InstructionCategoryID INT,
	SiteID INT NOT NULL,
    DescriptionEn VARCHAR(500) NOT NULL,
	DescriptionFr NVARCHAR(500),
	DescriptionEs NVARCHAR(500),
	Color VARCHAR(7) NOT NULL,
	InstructionGroupID INT NOT NULL,
	GroupDisplayOrder INT NOT NULL,
	GroupItemDisplayOrder INT NOT NULL,
	[Status] TINYINT DEFAULT(1) NOT NULL,
	[IsDeleted] BIT DEFAULT(0) NOT NULL,
	CreatedBy INT,
	CreatedOn DATETIME DEFAULT GETDATE() NOT NULL,
	UpdatedBy INT,
	UpdatedOn DATETIME,
	CONSTRAINT PK_InstructionMaster PRIMARY KEY (InstructionMasterID),
	CONSTRAINT FK_InstructionMasterInstructionCategory FOREIGN KEY (InstructionCategoryID) REFERENCES InstructionCategoryMaster(InstructionCategoryID),
	CONSTRAINT FK_InstructionMasterSiteMaster FOREIGN KEY (SiteID) REFERENCES SiteMaster(SiteID),
	CONSTRAINT FK_InstructionMasterInstructionGroupMaster FOREIGN KEY (InstructionGroupID) REFERENCES InstructionGroupMaster(InstructionGroupID)
);