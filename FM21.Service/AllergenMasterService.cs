﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using FM21.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FM21.Data;
using System.Runtime.CompilerServices;

namespace FM21.Service
{
    public class AllergenMasterService : IAllergenMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<AllergenMaster> allergenRepository;
        private readonly IValidator<AllergenMasterModel> validator;

        public AllergenMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            IValidator<AllergenMasterModel> validator, IRepository<AllergenMaster> allergenRepository)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.allergenRepository = allergenRepository;
            this.validator = validator;
        }

        public async Task<GeneralResponse<ICollection<AllergenMaster>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<AllergenMaster>>();
            try
            {
                response.Data = await allergenRepository.GetManyAsync(o => o.IsActive == true && o.IsDeleted == false);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<AllergenMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            filter = filter?.Trim().ToLower();
            var response = new PagedResult<AllergenMaster>();
            try
            {
                var data = allergenRepository.Query(true);
                //Filter
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    data = data.Where(x => x.AllergenName.ToLower().Contains(filter) || x.AllergenCode.ToLower().Contains(filter)
                                      || x.AllergenDescription_En.Contains(filter));
                }
                //sort 
                var ascending = (sortDirection == "asc");
                if (!string.IsNullOrWhiteSpace(sortColumn))
                {
                    switch (sortColumn.Trim().ToLower())
                    {
                        case "AllergenDescription_En":
                            data = data.OrderBy(p => p.AllergenDescription_En, ascending);
                            break;
                        case "AllergenCode":
                            data = data.OrderBy(p => p.AllergenCode, ascending);
                            break;
                        default:
                            data = data.OrderBy(p => p.AllergenName, ascending);
                            break;
                    }
                }
                //Page wise
                data = data.Where(i => i.IsActive == true && i.IsDeleted == false);
                response = await data.GetPaged(pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<AllergenMaster>> Create(AllergenMasterModel allergenMasterModel)
        {
            var response = new GeneralResponse<AllergenMaster>();
            try
            {
                var results = validator.Validate(allergenMasterModel, ruleSet: "New");

                if (results.IsValid)
                {
                    var isExists = allergenRepository.Any(x => x.AllergenName.ToLower().Trim() == allergenMasterModel.AllergenName.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        AllergenMaster allergenMaster = mapper.Map<AllergenMaster>(allergenMasterModel);
                        allergenMaster.IsActive = true;
                        allergenMaster.IsDeleted = false;
                        allergenMaster.CreatedOn = DateTime.Now;
                        allergenRepository.AddAsync(allergenMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<AllergenMaster>> Update(AllergenMasterModel allergenMasterModel)
        {
            var response = new GeneralResponse<AllergenMaster>();
            try
            {
                var results = validator.Validate(allergenMasterModel, ruleSet: "Edit,New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = allergenRepository.Any(x => x.AllergenID != allergenMasterModel.AllergenID
                                                            && x.AllergenName.ToLower() == allergenMasterModel.AllergenName.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        var obj = await allergenRepository.GetByIdAsync(allergenMasterModel.AllergenID);
                        if (obj != null)
                        {
                            mapper.Map<AllergenMasterModel, AllergenMaster>(allergenMasterModel, obj);
                            obj.UpdatedOn = DateTime.Now;
                            obj.IsDeleted = false;
                            obj.IsActive = true;
                            allergenRepository.UpdateAsync(obj);
                            await Save();
                            response.Message = localizer["msgUpdateSuccess"];
                        }
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await allergenRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsActive = false;
                        obj.IsDeleted = true;
                        allergenRepository.UpdateAsync(obj);
                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<AllergenMaster>> Get(int id)
        {
            var response = new GeneralResponse<AllergenMaster>();
            try
            {
                var obj = await allergenRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int Id)
        {
            //TODO: Check reference
            return false;
        }
    }
}
