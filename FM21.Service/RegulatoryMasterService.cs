﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using FM21.Service;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace FM21.Service
{

    public class RegulatoryMasterService : IRegulatoryMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<RegulatoryMaster> regulatoryMasterRepository;
        private readonly IRepository<NutrientMaster> nutrientRepository;
        private readonly IValidator<RegulatoryModel> validator;


        public RegulatoryMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            IRepository<RegulatoryMaster> regulatoryMasterRepository, IValidator<RegulatoryModel> validator, IRepository<NutrientMaster> nutrientRepository)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.regulatoryMasterRepository = regulatoryMasterRepository;
            this.validator = validator;
            this.nutrientRepository = nutrientRepository;
        }

        public async Task<GeneralResponse<ICollection<RegulatoryMaster>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<RegulatoryMaster>>();
            try
            {
                response.Data = await regulatoryMasterRepository.GetManyAsync(o => o.IsActive == true && o.IsDeleted == false);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<RegulatoryModel>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            filter = filter?.Trim().ToLower();
            var response = new PagedResult<RegulatoryModel>();
            try
            {
                var data = regulatoryMasterRepository.Query(true).Where(o => o.IsActive == true && o.IsDeleted == false)
               .Include(x => x.NutrientMaster)
               .Select(x => new RegulatoryModel
               {
                   RegulatoryId = x.RegulatoryId,
                   Nutrient = x.NutrientMaster.Name,
                   NutrientId = x.NutrientId,
                   OldUsa = x.OldUsa,
                   CanadaNi = x.CanadaNi,
                   CanadaNf = x.CanadaNf,
                   NewUsRdi = x.NewUsRdi,
                   EU = x.EU,
                   Unit = x.Unit,
                   UnitPerMg = x.UnitPerMg,
                   IsDeleted = x.IsDeleted,
                   IsActive = x.IsActive,
               })
               .AsQueryable();

                var ascending = sortDirection == "asc";
                if (!string.IsNullOrWhiteSpace(sortColumn))
                {
                    switch (sortColumn.Trim().ToLower())
                    {
                        case "nutrient":
                            data = data.OrderBy(p => p.Nutrient, ascending);
                            break;
                        case "OldUsa":
                            data = data.OrderBy(p => p.OldUsa, ascending);
                            break;
                        case "CanadaNi":
                            data = data.OrderBy(p => p.CanadaNi, ascending);
                            break;
                        case "CanadaNf":
                            data = data.OrderBy(p => p.CanadaNf, ascending);
                            break;
                        case "NewUsRdi":
                            data = data.OrderBy(p => p.NewUsRdi, ascending);
                            break;
                        case "EU":
                            data = data.OrderBy(p => p.EU, ascending);
                            break;
                        case "UnitPerMg":
                            data = data.OrderBy(p => p.UnitPerMg, ascending);
                            break;

                        default:
                            data = data.OrderBy(p => p.Nutrient, ascending);
                            break;
                    }
                }

                response = await data.GetPaged(pageIndex, pageSize);
                response.ExtraData = GetAllNutrientMaster(response);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);

            }
            return response;
        }

        public async Task<GeneralResponse<RegulatoryMaster>> Get(int id)
        {
            var response = new GeneralResponse<RegulatoryMaster>();
            try
            {
                var obj = await regulatoryMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<RegulatoryMaster>> Create(RegulatoryModel regulatoryMasterModel)
        {
            var response = new GeneralResponse<RegulatoryMaster>();
            try
            {
                var results = validator.Validate(regulatoryMasterModel, ruleSet: "New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = regulatoryMasterRepository.Any(x => x.NutrientId == regulatoryMasterModel.NutrientId);
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        RegulatoryMaster regulatoryMaster = mapper.Map<RegulatoryMaster>(regulatoryMasterModel);
                        regulatoryMaster.IsActive = true;
                        regulatoryMaster.IsDeleted = false;
                        regulatoryMasterRepository.AddAsync(regulatoryMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<RegulatoryMaster>> Update(RegulatoryModel regulatoryMasterModel)
        {
            var response = new GeneralResponse<RegulatoryMaster>();
            try
            {
                var results = validator.Validate(regulatoryMasterModel, ruleSet: "Edit,New");
                if (results.IsValid)
                {
                    //Unique validation
                    var isExists = regulatoryMasterRepository.Any(x => x.RegulatoryId != regulatoryMasterModel.RegulatoryId
                                                            && x.NutrientId == regulatoryMasterModel.NutrientId);
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        var obj = await regulatoryMasterRepository.GetByIdAsync(regulatoryMasterModel.RegulatoryId);
                    if (obj != null)
                    {
                        mapper.Map<RegulatoryModel, RegulatoryMaster>(regulatoryMasterModel, obj);
                        obj.UpdatedOn = DateTime.Now;
                        regulatoryMasterRepository.UpdateAsync(obj);
                        await Save();
                        response.Message = localizer["msgUpdateSuccess"];
                    }
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await regulatoryMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {

                    obj.UpdatedOn = DateTime.Now;
                    obj.IsActive = false;
                    obj.IsDeleted = true;
                    regulatoryMasterRepository.UpdateAsync(obj);
                    await Save();
                    response.Message = localizer["msgDeleteSuccess"];

                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }
        private List<KeyValuePair<string, string>> GetAllNutrientMaster(PagedResult<RegulatoryModel> response)
        {
            response.ExtraData = new List<KeyValuePair<string, string>>();
            var data = nutrientRepository.Query(true).Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
            foreach (var item in data)
            {
                response.ExtraData.Add(new KeyValuePair<string, string>(item.NutrientId.ToString(), item.Name));
            }

            return response.ExtraData;
        }
    }
}
