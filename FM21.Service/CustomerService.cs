﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Data.Repository;
using FM21.Entities;
using FM21.Service.Caching;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace FM21.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IValidator<CustomerModel> validator;
        private readonly ICustomerRepository customerRepository;

        public CustomerService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            ICustomerRepository customerRepository, IValidator<CustomerModel> validator)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.validator = validator;
            this.customerRepository = customerRepository;
        }

        public IEnumerable<Customer> GetAll(object[] parameters)
        {
            string spQuery = "exec dbo.[GetCustomerist] @FilterString,@SortString,@PageNumber,@PageSize,@TotalCount";
            Object[] abc =  { new SqlParameter("@FilterString", DbType.String){ Value=parameters[0] },
                             new SqlParameter("@SortString", DbType.String){ Value=parameters[1] },
                             new SqlParameter("@PageNumber", DbType.Int32 ){ Value=parameters[2] },
                             new SqlParameter("@PageSize",DbType.Int32){ Value=parameters[3] },
                             new SqlParameter("@TotalCount", DbType.Int32){ Direction=ParameterDirection.Output, Value=parameters[4] }
                             };
            return customerRepository.ExecuteQuery(spQuery, abc);
        }
        public async Task<GeneralResponse<ICollection<Customer>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<Customer>>();
            try
            {
                response.Data = await customerRepository.GetManyAsync(o => o.IsActive == true && o.IsDeleted == false);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<Customer>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            filter = filter?.Trim().ToLower();
            var response = new PagedResult<Customer>();
            try
            {
                var data = customerRepository.Query(true).Where(o => o.IsActive == true && o.IsDeleted == false);
                //Filter
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    data = data.Where(x => x.Name.ToLower().Contains(filter) || x.Address.ToLower().Contains(filter)
                                      || x.PhoneNumber.Contains(filter));
                }
                //sort 
                var ascending = sortDirection == "asc";
                if (!string.IsNullOrWhiteSpace(sortColumn))
                {
                    switch (sortColumn.Trim().ToLower())
                    {
                        case "name":
                            data = data.OrderBy(p => p.Name, ascending);
                            break;
                        case "address":
                            data = data.OrderBy(p => p.Address, ascending);
                            break;
                        case "email":
                            data = data.OrderBy(p => p.Email, ascending);
                            break;
                        //case "PhoneNumber":
                        //    data = data.OrderBy(p => p.PhoneNumber, ascending);
                        //    break;
                        default:
                            data = data.OrderBy(p => p.Name, ascending);
                            break;
                    }
                }
                //Page wise
                response = await data.GetPaged(pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<Customer>> Get(int id)
        {
            var response = new GeneralResponse<Customer>();
            try
            {
                var obj = await customerRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<Customer>> Create(CustomerModel objcustomerModel)
        {
            var response = new GeneralResponse<Customer>();
            try
            {
                var results = validator.Validate(objcustomerModel, ruleSet: "New");

                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = customerRepository.Any(x => x.Name.ToLower().Trim() == objcustomerModel.Name.ToLower().Trim()
                     || x.Email.ToLower().Trim() == objcustomerModel.Email.ToLower().Trim()
                     || x.PhoneNumber.Trim() == objcustomerModel.PhoneNumber.Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        Customer objcustomer = mapper.Map<Customer>(objcustomerModel);
                        objcustomer.IsActive = true;
                        objcustomer.IsDeleted = false;
                        customerRepository.AddAsync(objcustomer);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<Customer>> Update(CustomerModel objcustomerModel)
        {
            var response = new GeneralResponse<Customer>();
            try
            {

                var results = validator.Validate(objcustomerModel, ruleSet: "Edit,New");

                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = customerRepository.Any(x => x.CustomerId != objcustomerModel.CustomerId
                                                            && x.Name.ToLower() == objcustomerModel.Name.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        var obj = await customerRepository.GetByIdAsync(objcustomerModel.CustomerId);
                        if (obj != null)
                        {
                            mapper.Map<CustomerModel, Customer>(objcustomerModel, obj);
                            obj.UpdatedOn = DateTime.Now;
                            obj.IsDeleted = false;
                            obj.IsActive = true;
                            customerRepository.UpdateAsync(obj);
                            await Save();
                            response.Message = localizer["msgUpdateSuccess"];
                        }
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await customerRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    // check has reference 
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsActive = false;
                        obj.IsDeleted = true;
                        customerRepository.UpdateAsync(obj);
                        //objcustomerRepository.Delete(obj);
                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int Id)
        {
            //TODO: Check reference
            //** Need to check before customer delete that, Is customer Associated with formula
            return false;
        }
    }
}