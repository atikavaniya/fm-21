﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FM21.Service
{
    public class RoleMasterService : IRoleMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<RoleMaster> roleMasterRepository;
        private readonly IValidator<RoleMasterModel> validator;
        private readonly IRepository<RolePermissionMapping> rolePermissionRepository;

        public RoleMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            IRepository<RoleMaster> roleMasterRepository, IValidator<RoleMasterModel> validator, IRepository<RolePermissionMapping> rolePermissionRepository)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.roleMasterRepository = roleMasterRepository;
            this.validator = validator;
            this.rolePermissionRepository = rolePermissionRepository;
        }

        public async Task<GeneralResponse<ICollection<RoleMaster>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<RoleMaster>>();
            try
            {
                response.Data = await roleMasterRepository.GetManyAsync(o => o.IsActive && !o.IsDeleted);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<RoleMaster>> GetPageWiseData(SearchFilter searchFilter)
        {
            searchFilter.Search = searchFilter.Search?.Trim().ToLower();
            var response = new PagedResult<RoleMaster>();
            try
            {
                var data = roleMasterRepository.Query(true).Where(o => o.IsActive && !o.IsDeleted);
                //Filter
                if (!string.IsNullOrWhiteSpace(searchFilter.Search))
                {
                    data = data.Where(x => x.RoleName.ToLower().Contains(searchFilter.Search) || x.RoleDescription.ToLower().Contains(searchFilter.Search));
                }
                //sort 
                var ascending = searchFilter.SortDirection == "asc";
                if (!string.IsNullOrWhiteSpace(searchFilter.SortColumn))
                {
                    switch (searchFilter.SortColumn.Trim().ToLower())
                    {
                        case "rolename":
                            data = data.OrderBy(p => p.RoleName, ascending);
                            break;
                        case "roledescription":
                            data = data.OrderBy(p => p.RoleDescription, ascending);
                            break;
                        default:
                            data = data.OrderBy(p => p.RoleName, ascending);
                            break;
                    }
                }
                //Page wise
                response = await data.GetPaged(searchFilter.PageIndex, searchFilter.PageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<RoleMaster>> Get(int id)
        {
            var response = new GeneralResponse<RoleMaster>();
            try
            {
                var obj = await roleMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<RoleMaster>> Create(RoleMasterModel roleMasterModel)
        {
            var response = new GeneralResponse<RoleMaster>();
            try
            {
                var results = validator.Validate(roleMasterModel, ruleSet: "New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = roleMasterRepository.Any(x => x.RoleName.ToLower().Trim() == roleMasterModel.RoleName.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        RoleMaster roleMaster = mapper.Map<RoleMaster>(roleMasterModel);
                        roleMaster.IsActive = true;
                        roleMaster.IsDeleted = false;
                        roleMasterRepository.AddAsync(roleMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.SetInfo(results);
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<RoleMaster>> Update(RoleMasterModel roleMasterModel)
        {
            var response = new GeneralResponse<RoleMaster>();
            try
            {
                var results = validator.Validate(roleMasterModel, ruleSet: "Edit,New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = roleMasterRepository.Any(x => x.RoleID != roleMasterModel.RoleID
                                                            && x.RoleName.ToLower() == roleMasterModel.RoleName.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        var obj = await roleMasterRepository.GetByIdAsync(roleMasterModel.RoleID);
                        if (obj != null)
                        {
                            mapper.Map<RoleMasterModel, RoleMaster>(roleMasterModel, obj);
                            obj.UpdatedOn = DateTime.Now;
                            obj.IsActive = true;
                            obj.IsDeleted = false;
                            roleMasterRepository.UpdateAsync(obj);
                            await Save();
                            response.Message = localizer["msgUpdateSuccess"];
                        }
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.SetInfo(results);
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await roleMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = true;
                        obj.IsActive = false;
                        roleMasterRepository.UpdateAsync(obj);
                        //Delete all the permission which belongs to role
                        DeleteRolePermission(id);
                        //roleMasterRepository.Delete(obj);
                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int id)
        {
            return roleMasterRepository.Any(x => x.RoleID == id &&
                    (
                        x.UserRole.Any()
                    ));
        }

        private void DeleteRolePermission(int roleID)
        {
            rolePermissionRepository.Delete(o => o.RoleID == roleID);
        }
    }
}