﻿using AutoMapper;
using FM21.Core;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FM21.Service
{
    public class CommonService : ICommonService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<SiteMaster> siteMasterRepository;
        private readonly IRepository<InstructionCategoryMaster> instructionCategoryMasterRepository;
        private readonly IRepository<InstructionGroupMaster> instructionGroupMasterRepository;
        private readonly IRepository<InstructionSiteMapping> instructionSiteMappingRepository;

        public CommonService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            IRepository<SiteMaster> siteMasterRepository, IRepository<InstructionCategoryMaster> instructionCategoryMasterRepository, IRepository<InstructionGroupMaster> instructionGroupMasterRepository,
            IRepository<InstructionSiteMapping> instructionSiteMappingRepository)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.siteMasterRepository = siteMasterRepository;
            this.instructionCategoryMasterRepository = instructionCategoryMasterRepository;
            this.instructionGroupMasterRepository = instructionGroupMasterRepository;
            this.instructionSiteMappingRepository = instructionSiteMappingRepository;
        }

        public async Task<GeneralResponse<ICollection<SiteMaster>>> GetAllSite()
        {
            var response = new GeneralResponse<ICollection<SiteMaster>>();
            try
            {
                response.Data = await siteMasterRepository.GetManyAsync(o => o.IsActive);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<ICollection<InstructionCategoryMaster>>> GetAllInstructionCategory()
        {
            var response = new GeneralResponse<ICollection<InstructionCategoryMaster>>();
            try
            {
                response.Data = await instructionCategoryMasterRepository.GetManyAsync(o => o.IsActive);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<ICollection<InstructionCategoryMaster>>> GetAllInstructionCategoryBySiteID(int siteID)
        {
            var response = new GeneralResponse<ICollection<InstructionCategoryMaster>>();
            try
            {
               var result = await Task.Run<GeneralResponse<ICollection<InstructionCategoryMaster>>>(() =>
                {
                    response.Data = siteMasterRepository.Query(true)
                                                        .Where(o => o.SiteID == Convert.ToInt32(siteID))
                                                        .Include(o => o.InstructionSiteMapping)
                                                        .ThenInclude(o => o.InstructionCategory)
                                                        .FirstOrDefault().InstructionSiteMapping.
                                                        Select(o => new InstructionCategoryMaster 
                                                        {
                                                            InstructionCategoryID = o.InstructionCategory.InstructionCategoryID,
                                                            InstructionCategory = o.InstructionCategory.InstructionCategory
                                                        })
                                                        .ToList();
                    return response;
                }); 
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<ICollection<InstructionGroupMaster>>> GetAllInstructionGroup()
        {
            var response = new GeneralResponse<ICollection<InstructionGroupMaster>>();
            try
            {
                response.Data = await instructionGroupMasterRepository.GetManyAsync(o => o.IsActive);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }
    }
}