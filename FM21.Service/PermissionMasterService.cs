﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
namespace FM21.Service
{

    public class PermissionMasterService : IPermissionMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<PermissionMaster> permissionMasterRepository;
        private readonly IValidator<PermissionMasterModel> validator;
        private readonly IRepository<RoleMaster> roleRepository;
        private readonly IRepository<RolePermissionMapping> rolePermissionMapRepo;

        public PermissionMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider,
            IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
            IRepository<PermissionMaster> permissionMasterRepository, IValidator<PermissionMasterModel> validator,
            IRepository<RoleMaster> roleRepository, IRepository<RolePermissionMapping> rolePermissionMapRepo)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.permissionMasterRepository = permissionMasterRepository;
            this.validator = validator;
            this.roleRepository = roleRepository;
            this.rolePermissionMapRepo = rolePermissionMapRepo;
        }

        public async Task<GeneralResponse<ICollection<PermissionMaster>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<PermissionMaster>>();
            try
            {
                response.Data = await permissionMasterRepository.GetManyAsync(o => o.IsDeleted == false);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<PermissionMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            filter = filter?.Trim().ToLower();
            var response = new PagedResult<PermissionMaster>();
            try
            {
                var data = permissionMasterRepository.Query(true);
                //Filter
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    data = data.Where(x => x.PermissionFor.ToLower().Contains(filter));
                }
                //sort 
                var ascending = sortDirection == "asc";
                if (!string.IsNullOrWhiteSpace(sortColumn))
                {
                    switch (sortColumn.Trim().ToLower())
                    {
                        case "permissionfor":
                            data = data.OrderBy(p => p.PermissionFor, ascending);
                            break;

                        default:
                            data = data.OrderBy(p => p.PermissionFor, ascending);
                            break;
                    }
                }
                //Page wise
                response = await data.GetPaged(pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<PermissionMaster>> Get(int id)
        {
            var response = new GeneralResponse<PermissionMaster>();
            try
            {
                var obj = await permissionMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<PermissionMaster>> Create(PermissionMasterModel permissionMasterModel)
        {
            var response = new GeneralResponse<PermissionMaster>();
            try
            {
                var results = validator.Validate(permissionMasterModel, ruleSet: "all");

                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = permissionMasterRepository.Any(x => x.PermissionFor.ToLower().Trim() == permissionMasterModel.PermissionFor.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        PermissionMaster permissionMaster = mapper.Map<PermissionMaster>(permissionMasterModel);
                        permissionMaster.IsDeleted = false;
                        permissionMasterRepository.AddAsync(permissionMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<PermissionMaster>> Update(PermissionMasterModel permissionMasterModel)
        {
            var response = new GeneralResponse<PermissionMaster>();
            try
            {
                // Unique validation 
                var isExists = permissionMasterRepository.Any(x => x.PermissionID != permissionMasterModel.PermissionID
                                                            && x.PermissionFor.ToLower() == permissionMasterModel.PermissionFor.ToLower().Trim());
                if (isExists)
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgDuplicateRecord"];
                }
                else
                {
                    var obj = await permissionMasterRepository.GetByIdAsync(permissionMasterModel.PermissionID);
                    if (obj != null)
                    {
                        mapper.Map<PermissionMasterModel, PermissionMaster>(permissionMasterModel, obj);
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        permissionMasterRepository.UpdateAsync(obj);
                        await Save();
                        response.Message = localizer["msgUpdateSuccess"];
                    }
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await permissionMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    // check has reference 
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = true;
                        permissionMasterRepository.UpdateAsync(obj);

                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<ICollection<RolePermissionMatrix>>> GetRolePermissionMatrix()
        {

            var response = new GeneralResponse<ICollection<RolePermissionMatrix>>();
            try
            {

                var result = await Task.Run<GeneralResponse<ICollection<RolePermissionMatrix>>>(() =>
                {
                    response.Data = permissionMasterRepository.Query(true)
                         .Where(o => o.PermissionID != Convert.ToInt32(PermissionMasterEnum.ViewPrintformulareports)
                          && o.PermissionID != Convert.ToInt32(PermissionMasterEnum.ViewPrintingredientreports)
                          && o.IsDeleted == false)
                        .Include(o => o.RolePermissionMapping)
                        .ThenInclude(o => o.Role)
                   .Select(x => new RolePermissionMatrix
                   {
                       permissionId = x.PermissionID,
                       name = x.PermissionFor,

                       roleAccessList = (
                                    from rm in roleRepository.Query(true).
                                    Where(o => o.IsActive == true && o.IsDeleted == false
                                    && o.RoleName != RoleMasterEnum.Admin.ToString())
                                    join rmmapping in x.RolePermissionMapping
                                    on rm.RoleID equals rmmapping.RoleID into joinRoleMasterRolePermissionMap
                                    from dataResult in joinRoleMasterRolePermissionMap.DefaultIfEmpty()
                                    orderby rm.RoleID ascending
                                    select new RolePermissionAccess
                                    {
                                        roleID = rm.RoleID,
                                        roleName = rm.RoleName,
                                        isAccess = dataResult.Equals(null) || dataResult.PermissionType == 0 ? false : true,
                                        permissionID = dataResult.PermissionID,
                                        rolePermissionID = dataResult.RolePermissionID,// x.RolePermissionMapping.FirstOrDefault().RolePermissionID
                                    }).ToList()
                   })
                   .AsQueryable().ToList();

                    return response;
                });
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }
        public async Task<GeneralResponse<RolePermissionAccess>> UpdateRoleMatrix(List<RolePermissionAccess> roleAccessList)
        {
            var response = new GeneralResponse<RolePermissionAccess>();
            try
            {
                foreach (var item in roleAccessList)
                {
                    if (item.rolePermissionID > 0)
                    {

                        var obj = await rolePermissionMapRepo.GetByIdAsync(item.rolePermissionID);
                        if (obj != null)
                        {
                            obj.PermissionType = Convert.ToByte(item.isAccess.GetHashCode());
                            rolePermissionMapRepo.UpdateAsync(obj);
                        }
                    }
                    else
                    {
                        RolePermissionMapping rpMapping = mapper.Map<RolePermissionMapping>(item);
                        rolePermissionMapRepo.AddAsync(rpMapping);
                    }
                }
                await Save();
                response.Message = localizer["msgUpdateSuccess"];
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }


        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int Id)
        {
            //TODO: Check reference
            return false;
        }
    }
}
