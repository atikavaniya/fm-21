﻿using FM21.Core;
using FM21.Data.Repository;
using FM21.Entities;
using FM21.Service.Caching;
using System.Threading.Tasks;
using static FM21.Service.Caching.CacheProvider;

namespace FM21.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly ICacheProvider cacheProvider;

        public UserService(ICacheProvider cacheProvider, IUserRepository userRepository)
        {
            this.cacheProvider = cacheProvider;
            this.userRepository = userRepository;
        }
    }
}