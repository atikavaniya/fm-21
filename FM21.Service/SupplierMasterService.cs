﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using FM21.Service.Interface;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FM21.Data;

namespace FM21.Service
{
    public class SupplierMasterService : ISupplierMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<SupplierMaster> supplierMasterRepository;
        private readonly IValidator<SupplierMasterModel> validator;

        public SupplierMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler,
    IRepository<SupplierMaster> supplierMasterRepository, IValidator<SupplierMasterModel> validator)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.supplierMasterRepository = supplierMasterRepository;
            this.validator = validator;
        }

        public async Task<GeneralResponse<SupplierMaster>> Create(SupplierMasterModel supplierMasterModel)
        {
            var response = new GeneralResponse<SupplierMaster>();
            try
            {
                var results = validator.Validate(supplierMasterModel, ruleSet: "New");


                if (results.IsValid)
                {
                    var isExists = supplierMasterRepository.Any(x => x.SupplierName.ToLower().Trim() == supplierMasterModel.SupplierName.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        SupplierMaster supplierMaster = mapper.Map<SupplierMaster>(supplierMasterModel);
                        supplierMaster.IsActive = true;
                        supplierMaster.IsDeleted = false;
                        supplierMaster.CreatedOn = DateTime.Now;
                        supplierMasterRepository.AddAsync(supplierMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await supplierMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    // check has reference 
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsActive = false;
                        obj.IsDeleted = true;
                        supplierMasterRepository.UpdateAsync(obj);
                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<SupplierMaster>> Get(int id)
        {
            var response = new GeneralResponse<SupplierMaster>();
            try
            {
                var obj = await supplierMasterRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<ICollection<SupplierMaster>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<SupplierMaster>>();
            try
            {
                //var data = cacheProvider.GetFromCache<ICollection<SupplierMaster>>(CachingKeys.SupplierMaster);
                //if (data != null)
                //{
                //    response.Data = data;
                //}
                //else
                {
                    response.Data = await supplierMasterRepository.GetManyAsync(o => o.IsDeleted == false && o.IsActive == true);
                    //cacheProvider.SetCache(CachingKeys.SupplierMaster, response.Data);
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<SupplierMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            filter = filter?.Trim().ToLower();
            var response = new PagedResult<SupplierMaster>();
            try
            {
                var data = supplierMasterRepository.Query(true);
                //Filter
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    data = data.Where(x => x.SupplierName.ToLower().Contains(filter) || x.Address.ToLower().Contains(filter) ||
                    x.PhoneNumber.Contains(filter));
                }
                //sort 
                var ascending = (sortDirection == "asc");
                if (!string.IsNullOrWhiteSpace(sortColumn))
                {
                    switch (sortColumn.Trim().ToLower())
                    {
                        case "SupplierName":
                            data = data.OrderBy(p => p.SupplierName, ascending);
                            break;
                        case "Address":
                            data = data.OrderBy(p => p.Address, ascending);
                            break;
                        case "Email":
                            data = data.OrderBy(p => p.Email, ascending);
                            break;
                        default:
                            data = data.OrderBy(p => p.SupplierName, ascending);
                            break;
                    }
                }
                //Page wise
                data = data.Where(i => i.IsActive == true && i.IsDeleted == false);
                response = await data.GetPaged(pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<SupplierMaster>> Update(SupplierMasterModel supplierMasterModel)
        {
            var response = new GeneralResponse<SupplierMaster>();
            try
            {
                var results = validator.Validate(supplierMasterModel, ruleSet: "Edit,New");
                if (results.IsValid)
                {
                    var obj = await supplierMasterRepository.GetByIdAsync(supplierMasterModel.SupplierId);
                    if (obj != null)
                    {
                        mapper.Map<SupplierMasterModel, SupplierMaster>(supplierMasterModel, obj);
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = false;
                        obj.IsActive = true;
                        supplierMasterRepository.UpdateAsync(obj);

                        await Save();
                        response.Message = localizer["msgUpdateSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.ExtraData = new List<KeyValuePair<string, string>>();
                    results.Errors.All(err =>
                    {
                        response.ExtraData.Add(new KeyValuePair<string, string>(err.PropertyName, err.ErrorMessage));
                        return true;
                    });
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int Id)
        {
            //TODO: Check reference
            return false;
        }
    }
}
