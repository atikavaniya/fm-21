﻿using FM21.Core;
using FM21.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.Service
{
    public interface ICommonService
    {
        Task<GeneralResponse<ICollection<SiteMaster>>> GetAllSite();
        Task<GeneralResponse<ICollection<InstructionCategoryMaster>>> GetAllInstructionCategory();
        Task<GeneralResponse<ICollection<InstructionCategoryMaster>>> GetAllInstructionCategoryBySiteID(int siteID);
        Task<GeneralResponse<ICollection<InstructionGroupMaster>>> GetAllInstructionGroup();
    }
}