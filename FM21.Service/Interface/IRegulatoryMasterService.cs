﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Service
{
  

    public interface IRegulatoryMasterService
    {
        Task<GeneralResponse<ICollection<RegulatoryMaster>>> GetAll();
        Task<PagedResult<RegulatoryModel>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection);
        Task<GeneralResponse<RegulatoryMaster>> Get(int id);
        Task<GeneralResponse<RegulatoryMaster>> Create(RegulatoryModel entity);
        Task<GeneralResponse<RegulatoryMaster>> Update(RegulatoryModel entity);
        Task<GeneralResponse<bool>> Delete(int id);

    }
}
