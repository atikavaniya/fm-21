﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.Service
    {
   
    public interface IPermissionMasterService
    {
        Task<GeneralResponse<ICollection<PermissionMaster>>> GetAll();
        Task<PagedResult<PermissionMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection);
        Task<GeneralResponse<PermissionMaster>> Get(int id);
        Task<GeneralResponse<PermissionMaster>> Create(PermissionMasterModel entity);
        Task<GeneralResponse<PermissionMaster>> Update(PermissionMasterModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
        Task<GeneralResponse<ICollection<RolePermissionMatrix>>> GetRolePermissionMatrix();
        Task<GeneralResponse<RolePermissionAccess>> UpdateRoleMatrix(List<RolePermissionAccess> roleAccessList);
    }
}
