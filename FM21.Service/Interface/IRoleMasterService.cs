﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.Service
{
    public interface IRoleMasterService
    {
        Task<GeneralResponse<ICollection<RoleMaster>>> GetAll();
        Task<PagedResult<RoleMaster>> GetPageWiseData(SearchFilter searchFilter);
        Task<GeneralResponse<RoleMaster>> Get(int id);
        Task<GeneralResponse<RoleMaster>> Create(RoleMasterModel entity);
        Task<GeneralResponse<RoleMaster>> Update(RoleMasterModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
    }
}