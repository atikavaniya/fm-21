﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Service.Interface
{
    public interface ISupplierMasterService
    {

        Task<GeneralResponse<ICollection<SupplierMaster>>> GetAll();
        Task<PagedResult<SupplierMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection);
        Task<GeneralResponse<SupplierMaster>> Get(int id);
        Task<GeneralResponse<SupplierMaster>> Create(SupplierMasterModel entity);
        Task<GeneralResponse<SupplierMaster>> Update(SupplierMasterModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
    }
}
