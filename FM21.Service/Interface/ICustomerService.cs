﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Service
{
    public interface ICustomerService
    {

        IEnumerable<Customer> GetAll(object[] parameters);
        Task<GeneralResponse<ICollection<Customer>>> GetAll();
        Task<PagedResult<Customer>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection);
        Task<GeneralResponse<Customer>> Get(int id);
        Task<GeneralResponse<Customer>> Create(CustomerModel entity);
        Task<GeneralResponse<Customer>> Update(CustomerModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
    }
}
