﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FM21.Service
{
    public interface IInstructionMasterService
    {
        Task<GeneralResponse<ICollection<InstructionMasterModel>>> GetAll();
        Task<PagedResult<InstructionMasterModel>> GetPageWiseData(SearchFilter searchFilter);
        Task<GeneralResponse<InstructionMaster>> Get(int id);
        Task<GeneralResponse<InstructionMaster>> Create(InstructionMasterModel entity);
        Task<GeneralResponse<InstructionMaster>> Update(InstructionMasterModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
    }
}