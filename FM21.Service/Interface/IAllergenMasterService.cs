﻿using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Service.Interface
{
    public interface IAllergenMasterService
    {
        Task<GeneralResponse<ICollection<AllergenMaster>>> GetAll();
        Task<GeneralResponse<AllergenMaster>> Create(AllergenMasterModel entity);
        Task<GeneralResponse<AllergenMaster>> Update(AllergenMasterModel entity);
        Task<GeneralResponse<bool>> Delete(int id);
        Task<GeneralResponse<AllergenMaster>> Get(int id);
        Task<PagedResult<AllergenMaster>> GetPageWiseData(string filter, int pageIndex, int pageSize, string sortColumn, string sortDirection);
    }
}
