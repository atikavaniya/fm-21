﻿using AutoMapper;
using FluentValidation;
using FM21.Core;
using FM21.Core.Model;
using FM21.Data;
using FM21.Data.Infrastructure;
using FM21.Entities;
using FM21.Service.Caching;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace FM21.Service
{
    public class InstructionMasterService : IInstructionMasterService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheProvider cacheProvider;
        private readonly IMapper mapper;
        private readonly IStringLocalizer localizer;
        private readonly IExceptionHandler exceptionHandler;
        private readonly IRepository<InstructionMaster> instructionRepository;
        private readonly IValidator<InstructionMasterModel> validator;

        public InstructionMasterService(IUnitOfWork unitOfWork, ICacheProvider cacheProvider, IMapper mapper, IStringLocalizer localizer, IExceptionHandler exceptionHandler, 
            IRepository<InstructionMaster> instructionRepository, IValidator<InstructionMasterModel> validator)
        {
            this.unitOfWork = unitOfWork;
            this.cacheProvider = cacheProvider;
            this.mapper = mapper;
            this.localizer = localizer;
            this.exceptionHandler = exceptionHandler;
            this.instructionRepository = instructionRepository;
            this.validator = validator;
        }

        public async Task<GeneralResponse<ICollection<InstructionMasterModel>>> GetAll()
        {
            var response = new GeneralResponse<ICollection<InstructionMasterModel>>();
            ICollection<InstructionMasterModel> lstInstructions = new Collection<InstructionMasterModel>();
            try
                {
                var result = await Task.Run<GeneralResponse<ICollection<InstructionMasterModel>>>(() =>
                {
                    var arrList = instructionRepository.Query(true)
                                   .Where(o => o.IsActive && !o.IsDeleted)
                                   .Include(o => o.InstructionGroup)
                                   .Include(o => o.InstructionCategory)
                                   .ToList();

                    response.Data = mapper.Map<IList<InstructionMaster>, IList<InstructionMasterModel>>(arrList);
                    return response;
                });
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<PagedResult<InstructionMasterModel>> GetPageWiseData(SearchFilter searchFilter)
        {
            searchFilter.Search = searchFilter.Search?.Trim().ToLower();
            var response = new PagedResult<InstructionMasterModel>();
            ICollection<InstructionMasterModel> lstInstructions = new Collection<InstructionMasterModel>();
            try
            {
                var result = instructionRepository.Query(true)
                                .Where(o => o.IsActive && !o.IsDeleted);

                //Filter
                if (!string.IsNullOrWhiteSpace(searchFilter.Search))
                {
                    result = result.Where(x => x.DescriptionEn.ToLower().Contains(searchFilter.Search) 
                                        || x.DescriptionFr.ToLower().Contains(searchFilter.Search) 
                                        || x.DescriptionEs.ToLower().Contains(searchFilter.Search));
                }

                var arrData = result.Include(o => o.InstructionGroup)
                                    .Include(o => o.InstructionCategory)
                                    .ToList();

                var data = mapper.Map<IList<InstructionMaster>, IList<InstructionMasterModel>>(arrData).AsQueryable<InstructionMasterModel>();

                //sort 
                var ascending = searchFilter.SortDirection == "asc";
                if (!string.IsNullOrWhiteSpace(searchFilter.SortColumn))
                {
                    switch (searchFilter.SortColumn.Trim().ToLower())
                    {
                        case "site":
                            data = data.OrderBy(p => p.Site, ascending);
                            break;
                        case "instructioncategory":
                            data = data.OrderBy(p => p.InstructionCategory, ascending);
                            break;
                        case "instructiongroup":
                            data = data.OrderBy(p => p.InstructionGroup, ascending);
                            break;
                        case "descriptionen":
                            data = data.OrderBy(p => p.DescriptionEn, ascending);
                            break;
                        case "descriptionfr":
                            data = data.OrderBy(p => p.DescriptionFr, ascending);
                            break;
                        case "descriptiones":
                            data = data.OrderBy(p => p.DescriptionEs, ascending);
                            break;
                        default:
                            data = data.OrderBy(p => p.DescriptionEn, ascending);
                            break;
                    }
                }
                
                //Page wise
                response = await data.GetPaged(searchFilter.PageIndex, searchFilter.PageSize);
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<InstructionMaster>> Get(int id)
        {
            var response = new GeneralResponse<InstructionMaster>();
            try
            {
                var obj = await instructionRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    response.Data = obj;
                }
                else
                {
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<InstructionMaster>> Create(InstructionMasterModel instructionModel)
        {
            var response = new GeneralResponse<InstructionMaster>();
            try
            {
                var results = validator.Validate(instructionModel, ruleSet: "New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = instructionRepository.Any(x => x.DescriptionEn.ToLower().Trim() == instructionModel.DescriptionEn.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        InstructionMaster instructionMaster = mapper.Map<InstructionMaster>(instructionModel);
                        instructionMaster.IsActive = true;

                        var groupData = await instructionRepository.GetManyAsync(o => o.InstructionMasterID == instructionMaster.InstructionGroupID);
                        if(groupData.FirstOrDefault() == null)
                        {
                            int groupDisplayOrder = (await instructionRepository.GetAllAsync()).Select(o => o.GroupDisplayOrder).DefaultIfEmpty().Max() + 1;
                            instructionMaster.GroupDisplayOrder = groupDisplayOrder;
                        }
                        else
                        {
                            instructionMaster.GroupDisplayOrder = groupData.FirstOrDefault().GroupDisplayOrder + 1;
                        }
                        instructionMaster.GroupItemDisplayOrder = groupData.Select(o => o.GroupItemDisplayOrder).DefaultIfEmpty().Max() + 1;
                        instructionRepository.AddAsync(instructionMaster);
                        await Save();
                        response.Message = localizer["msgInsertSuccess"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.SetInfo(results);
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<InstructionMaster>> Update(InstructionMasterModel instructionModel)
        {
            var response = new GeneralResponse<InstructionMaster>();
            try
            {
                var results = validator.Validate(instructionModel, ruleSet: "Edit,New");
                if (results.IsValid)
                {
                    // Unique validation 
                    var isExists = instructionRepository.Any(x => x.InstructionMasterID != instructionModel.InstructionMasterID
                                                            && x.DescriptionEn.ToLower() == instructionModel.DescriptionEn.ToLower().Trim());
                    if (isExists)
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDuplicateRecord"];
                    }
                    else
                    {
                        var obj = await instructionRepository.GetByIdAsync(instructionModel.InstructionMasterID);
                        if (obj != null)
                        {
                            mapper.Map<InstructionMasterModel, InstructionMaster>(instructionModel, obj);
                            obj.UpdatedOn = DateTime.Now;
                            instructionRepository.UpdateAsync(obj);
                            await Save();
                            response.Message = localizer["msgUpdateSuccess"];
                        }
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.SetInfo(results);
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        public async Task<GeneralResponse<bool>> Delete(int id)
        {
            var response = new GeneralResponse<bool>();
            try
            {
                var obj = await instructionRepository.GetByIdAsync(id);
                if (obj != null)
                {
                    // check has reference 
                    if (!HasReference(id))
                    {
                        obj.UpdatedOn = DateTime.Now;
                        obj.IsDeleted = true;
                        instructionRepository.UpdateAsync(obj);
                        //instructionRepository.Delete(obj);
                        await Save();
                        response.Message = localizer["msgDeleteSuccess"];
                    }
                    else
                    {
                        response.Result = ResultType.Warning;
                        response.Message = localizer["msgDeleteFailAsUsedByOthers"];
                    }
                }
                else
                {
                    response.Result = ResultType.Warning;
                    response.Message = localizer["msgRecordNotExist"];
                }
            }
            catch (Exception ex)
            {
                response.Result = ResultType.Error;
                response.Exception = ex;
                exceptionHandler.LogError(ex);
            }
            return response;
        }

        private async Task<int> Save()
        {
            return await unitOfWork.CommitAsync();
        }

        public bool HasReference(int Id)
        {
            //TODO: Check reference
            return false;
        }
    }
}