﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FM21.Entities
{
    [Table("SiteMaster")]
    public class SiteMaster
    {
        public SiteMaster()
        {
            InstructionMaster = new HashSet<InstructionMaster>();
            InstructionSiteMapping = new HashSet<InstructionSiteMapping>();
        }

        [Key]
        public int SiteID { get; set; }
        public string SiteCode { get; set; }
        public string SiteDescription { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<InstructionMaster> InstructionMaster { get; set; }

        public virtual ICollection<InstructionSiteMapping> InstructionSiteMapping { get; set; }
    }
}