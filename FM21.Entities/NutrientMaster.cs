﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FM21.Entities
{
   

    [Table("NutrientMaster")]
    public class NutrientMaster
    {
        public NutrientMaster()
        {
            NutrientTypeMaster = new HashSet<NutrientTypeMaster>();
        }

        [Key]
        public int NutrientId { get; set; }
        
        public string Name { get; set; }
        public int NutrientTypeId { get; set; }
        public bool IsShowOnTarget { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual ICollection<NutrientTypeMaster> NutrientTypeMaster { get; set; }
        public virtual ICollection<RegulatoryMaster> RegulatoryMaster { get; set; }
    }
}
