﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FM21.Entities
{
   
    [Table("NutrientTypeMaster")]
    public class NutrientTypeMaster
    {
        [Key]
        public int TypeName { get; set; }    
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public virtual NutrientMaster NutrientMaster { get; set; }
    }
}
