﻿using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Tests
{
    public class AllergenMasterTest
    {
        private Mock<IAllergenMasterService> allergenMasterService;
        GeneralResponse<ICollection<AllergenMaster>> lstAllergen;
        private AllergenMasterController allergenMasterController;
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        Mock<IAllergenMasterService> allergenData;
        
        [SetUp]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            allergenMasterService = new Mock<IAllergenMasterService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                     m.PageSize == 10 &&
                     m.PageIndex == 1 &&
                     m.Search == "a" && m.SortColumn == "AllergenCode" && m.SortDirection == "asc");
        }

        [Test]
        public void Test_Get_AllAllergen()
        {

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenMasterService.Object);
            var allergenList = allergenMasterController.GetAll();
            Assert.IsNotNull(allergenList);
            Assert.That(allergenList, Is.TypeOf<System.Threading.Tasks.Task<IActionResult>>()); //Tests exact type

        }

        [Test]
        public async Task Test_GetSearchList_OkResult()
        {
            allergenMasterController = new AllergenMasterController(localizer.Object, allergenMasterService.Object);
            var supplierList = await allergenMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNotNull(supplierList);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)supplierList.StatusCode);
        }

        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            allergenMasterController = new AllergenMasterController(localizer.Object, allergenMasterService.Object);
            var supplierList = await allergenMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(supplierList);
            //Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)customerList.StatusCode);
        }

        [Test]
        public async Task Test_GetAllergenAsync_With_ValidAllergenId()
        {
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr= "Crustacés"
                }
            };

            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Get(objAllergen.Data.AllergenID)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenList = await allergenMasterController.GetAllergen(objAllergen.Data.AllergenID) as OkObjectResult;
            var actualResult = allergenList.Value;

            allergenData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(objAllergen.Data.AllergenID, ((GeneralResponse<AllergenMaster>)actualResult).Data.AllergenID);
            Assert.AreEqual(objAllergen.Data.AllergenName, ((GeneralResponse<AllergenMaster>)actualResult).Data.AllergenName);
            Assert.AreEqual(objAllergen.Data.AllergenCode, ((GeneralResponse<AllergenMaster>)actualResult).Data.AllergenCode);
            Assert.AreEqual(objAllergen.Data.AllergenDescription_En, ((GeneralResponse<AllergenMaster>)actualResult).Data.AllergenDescription_En);
            Assert.AreEqual(objAllergen.Data.AllergenDescription_Fr, ((GeneralResponse<AllergenMaster>)actualResult).Data.AllergenDescription_Fr);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)allergenList.StatusCode);

        }

        [Test]
        public async Task Test_GetAllergenAsync_With_InValidAllergenId()
        {
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1220,
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Get(objAllergen.Data.AllergenID)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenObject = await allergenMasterController.GetAllergen(objAllergen.Data.AllergenID) as NotFoundResult;

            allergenData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(allergenObject);

        }

        [Test]
        public async Task Test_CreateAllergenAsync_With_ValidData()
        {
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            GeneralResponse<AllergenMasterModel> objAllergenModel = new GeneralResponse<AllergenMasterModel>()
            {
                Data = new AllergenMasterModel()
                {
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };


            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Create(objAllergenModel.Data)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenObject = await allergenMasterController.GetAllergen(objAllergen.Data.AllergenID) as OkObjectResult;

            allergenData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNotNull(allergenData);
        }

        [Test]
        public async Task Test_CreateAllergenAsync_With_InValidData()
        {
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "LDDDDDDDDDDDDDDDDDDDD",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            GeneralResponse<AllergenMasterModel> objAllergenModel = new GeneralResponse<AllergenMasterModel>()
            {
                Data = new AllergenMasterModel()
                {
                    AllergenName = "Shrimp",
                    AllergenCode = "LDDDDDDDDDDDDDDDDDDDD",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };


            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Create(objAllergenModel.Data)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenObject = await allergenMasterController.PostAllergen(objAllergenModel.Data);

            Assert.IsNull(allergenObject.Value);
        }
        [Test]
        public async Task Test_UpdateAllergenAsync_With_ValidData()
        {
            int AllergenID = 10;
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            GeneralResponse<AllergenMasterModel> objAllergenModel = new GeneralResponse<AllergenMasterModel>()
            {
                Data = new AllergenMasterModel()
                {
                    AllergenID=1,
                    AllergenName = "Shrimp",
                    AllergenCode = "L",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };
            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Update(objAllergenModel.Data)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenObject = await allergenMasterController.PutAllergen(AllergenID, objAllergenModel.Data) as JsonResult;

            Assert.That(
            allergenObject.Value,
            Is.EqualTo(objAllergen));
            Assert.IsNotNull(allergenObject);
        }

        [Test]
        public async Task Test_UpdateAllergenAsync_With_InValidData()
        {
            int AllergenID = 1;
            GeneralResponse<AllergenMaster> objAllergen = new GeneralResponse<AllergenMaster>()
            {
                Data = new AllergenMaster()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "LDDDDDDDDDDDDDDDDDDDDDD",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            GeneralResponse<AllergenMasterModel> objAllergenModel = new GeneralResponse<AllergenMasterModel>()
            {
                Data = new AllergenMasterModel()
                {
                    AllergenID = 1,
                    AllergenName = "Shrimp",
                    AllergenCode = "LDDDDDDDDDDDDDDDDDDDDDD",
                    AllergenDescription_En = "Crustacean",
                    AllergenDescription_Fr = "Crustacés"
                }
            };

            allergenData = new Mock<IAllergenMasterService>();
            allergenData.Setup(t => t.Get(objAllergen.Data.AllergenID)).ReturnsAsync(objAllergen);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var allergenObject = await allergenMasterController.PutAllergen(AllergenID, objAllergenModel.Data) as JsonResult;

            Assert.IsNull(allergenObject.Value);
        }
        [Test]
        public async Task Test_DeleteAllergen_OkResult()
        {
            GeneralResponse<bool> response = new GeneralResponse<bool>();
            allergenData = new Mock<IAllergenMasterService>();

            allergenData.Setup(t => t.Delete(1)).ReturnsAsync(response);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var result = await allergenMasterController.DeleteAllergen(1) as JsonResult;
            Assert.IsNotNull(result.Value);


        }

        [Test]
        public async Task Test_DeleteSupplier_NotFoundResult()
        {
            GeneralResponse<bool> response = new GeneralResponse<bool>();
            allergenData = new Mock<IAllergenMasterService>();

            allergenData.Setup(t => t.Delete(100)).ReturnsAsync(response);

            allergenMasterController = new AllergenMasterController(localizer.Object, allergenData.Object);

            var result = await allergenMasterController.DeleteAllergen(100) as JsonResult;
            Assert.IsNull(result.Value);


        }
    }
}
