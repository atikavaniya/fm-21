﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
namespace FM21.Tests
{
    public class CustomerTest
    {
        private Mock<ICustomerService> customerService;

        private CustomerController custController;
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        Mock<ICustomerService> mydata;
        [SetUp]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            customerService = new Mock<ICustomerService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                     m.PageSize == 10 &&
                     m.PageIndex == 1 &&
                     m.Search == "FM" &&
                     m.SortColumn == "" &&
                     m.SortDirection == "");
        }

        [Test]
        public async Task Test_Get_AllCustomer()
        {

            var response = getGeneralCustomerMockData();
            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.GetAll()).ReturnsAsync(response);

            custController = new CustomerController(localizer.Object, mydata.Object);
            var customerList = await custController.GetAll() as JsonResult;
            Assert.IsNotNull(customerList);           
            Assert.AreEqual(response.Data, ((GeneralResponse<ICollection<Customer>>)customerList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((GeneralResponse<ICollection<Customer>>)customerList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((GeneralResponse<ICollection<Customer>>)customerList.Value).Result);


        }
        [Test]
        public async Task Test_GetSearchList_OkResult()
        {


            var response = getCustomerMockData();
            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.GetPageWiseData(searchFilter.Search, searchFilter.PageIndex, searchFilter.PageSize, searchFilter.SortColumn, searchFilter.SortDirection)).ReturnsAsync(response);

            custController = new CustomerController(localizer.Object, mydata.Object);
            var customerList = await custController.GetSearchList(searchFilter) as OkObjectResult;
            Assert.IsNotNull(customerList);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)customerList.StatusCode);
            Assert.AreEqual(response.Data, ((PagedResult<Customer>)customerList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((PagedResult<Customer>)customerList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((PagedResult<Customer>)customerList.Value).Result);

        }
        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            custController = new CustomerController(localizer.Object, customerService.Object);
            var customerList = await custController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(customerList);
           
        }

        [Test]
        public async Task Test_GetCustomerAsync_With_ValidCustomerId()
        {
            GeneralResponse<Customer> objCustomer = new GeneralResponse<Customer>()
            {
                Data = new Customer()
                {
                    CustomerId = 2,
                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };

            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.Get(objCustomer.Data.CustomerId)).ReturnsAsync(objCustomer);

            custController = new CustomerController(localizer.Object, mydata.Object);

            var customerList = await custController.GetCustomer(objCustomer.Data.CustomerId) as OkObjectResult;
            var actualResult = customerList.Value;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(objCustomer.Data.Address, ((GeneralResponse<Customer>)actualResult).Data.Address);
            Assert.AreEqual(objCustomer.Data.PhoneNumber, ((GeneralResponse<Customer>)actualResult).Data.PhoneNumber);
            Assert.AreEqual(objCustomer.Data.Email, ((GeneralResponse<Customer>)actualResult).Data.Email);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)customerList.StatusCode);

        }

        [Test]
        public async Task Test_GetCustomerAsync_With_InValidCustomerId()
        {
            
            GeneralResponse<Customer> objCustomer = new GeneralResponse<Customer>()
            {
                Data = new Customer()
                {
                    CustomerId = 1520,
                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };

            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.Get(objCustomer.Data.CustomerId)).ReturnsAsync(objCustomer);

            custController = new CustomerController(localizer.Object, mydata.Object);

            var customerObject = await custController.GetCustomer(objCustomer.Data.CustomerId) as NotFoundResult;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(customerObject);
        }

        [Test]
        public async Task Test_CreateCustomerAsync()
        {
            GeneralResponse<Customer> objCustomer = new GeneralResponse<Customer>()
            {
                Data = new Customer()
                {

                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };
            GeneralResponse<CustomerModel> objCustomer1 = new GeneralResponse<CustomerModel>()
            {
                Data = new CustomerModel()
                {

                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };


            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.Create(objCustomer1.Data)).ReturnsAsync(objCustomer);

            custController = new CustomerController(localizer.Object, mydata.Object);

            var customerObject = await custController.PostCustomer(objCustomer1.Data);
            Assert.That(customerObject, Is.TypeOf<ActionResult<Customer>>()); //return true
            Assert.IsNotNull(customerObject);
        }

        [Test]
        public async Task Test_UpdateCustomerAsync()
        {
            int CustomerId = 1;
            GeneralResponse<Customer> objCustomer = new GeneralResponse<Customer>()
            {
                Data = new Customer()
                {
                    CustomerId = 1,
                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };
            GeneralResponse<CustomerModel> objCustomer1 = new GeneralResponse<CustomerModel>()
            {
                Data = new CustomerModel()
                {

                    Address = "FM Nessloson",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com"
                }
            };
            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.Update(objCustomer1.Data)).ReturnsAsync(objCustomer);

            custController = new CustomerController(localizer.Object, mydata.Object);

            var customerObject = await custController.PutCustomer(CustomerId, objCustomer1.Data) as JsonResult;

            Assert.That(
            customerObject.Value,
            Is.EqualTo(objCustomer));
            Assert.IsNotNull(customerObject);
        }

        [Test]
        public async Task Test_DeleteCustomerAsync()
        {
            int CustomerId = 1;
            var response = new GeneralResponse<bool>();

            mydata = new Mock<ICustomerService>();
            mydata.Setup(t => t.Delete(CustomerId)).ReturnsAsync(response);

            custController = new CustomerController(localizer.Object, mydata.Object);

            var customerObject = await custController.DeleteCustomer(CustomerId) as JsonResult;

            Assert.That(
            customerObject.Value,
            Is.EqualTo(response));
            Assert.IsNotNull(customerObject);
        }

     private PagedResult<Customer> getCustomerMockData()
        {
            var response = new PagedResult<Customer>();
            List<Customer> customerPermissionMatrix = new List<Customer>();
            customerPermissionMatrix.Add(new Customer()
            {
                CustomerId = 2,
                Name = "FM Nessloson 1",
                Email = "FM@gmail.com",
                Address = "address",
                PhoneNumber = "12345678",
                IsActive = true,
                IsDeleted=false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,
                CustomerAbbreviation1 = null,
                CustomerAbbreviation2 = null
            });
            customerPermissionMatrix.Add(new Customer()
            {
                CustomerId = 2,
                Name = "FM Nessloson 3",
                Email = "FM@gmail.com",
                Address = "address",
                PhoneNumber = "12345678",
                IsActive = true,
                IsDeleted = false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,
                CustomerAbbreviation1 = "CustomerAbbreviation1 Value",
                CustomerAbbreviation2 = "CustomerAbbreviation1 Value"
            });
            response.Data = customerPermissionMatrix;
            response.CurrentPage = 1;
            response.PageCount = 1;
            response.PageSize = 10;
            response.RowCount = 2;
            response.Message = null;
            response.Exception = null;
            response.Result = ResultType.Success;
            response.ExtraData = null;

            return response;
        }
       private GeneralResponse<ICollection<Customer>> getGeneralCustomerMockData()
        {
            var response = new GeneralResponse<ICollection<Customer>>();
            List<Customer> customerPermissionMatrix = new List<Customer>();
            customerPermissionMatrix.Add(new Customer()
            {
                CustomerId = 2,
                Name = "FM Nessloson 1",
                Email = "FM@gmail.com",
                Address = "address",
                PhoneNumber = "12345678",
                IsActive = true,
                IsDeleted = false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,
                CustomerAbbreviation1 = null,
                CustomerAbbreviation2 = null
            });
            customerPermissionMatrix.Add(new Customer()
            {
                CustomerId = 2,
                Name = "FM Nessloson 3",
                Email = "FM@gmail.com",
                Address = "address",
                PhoneNumber = "12345678",
                IsActive = true,
                IsDeleted = false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,
                CustomerAbbreviation1 = "CustomerAbbreviation1 Value",
                CustomerAbbreviation2 = "CustomerAbbreviation1 Value"
            });
            response.Data = customerPermissionMatrix;          

            return response;
        }
    }


}


