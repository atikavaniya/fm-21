﻿using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace FM21.Tests
{
    public class InstructionMasterTest
    {
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        private Mock<IInstructionMasterService> instructionMasterService;
        private InstructionMasterController instructionMasterController;
        Mock<IInstructionMasterService> mydata;

        [SetUp]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            instructionMasterService = new Mock<IInstructionMasterService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                 m.PageSize == 10 &&
                 m.PageIndex == 1 &&
                 m.Search == "test" &&
                 m.SortColumn == "" &&
                 m.SortDirection == "");
        }

        [Test]
        public async Task Test_Get_All()
        {
            var response = GetGeneralinstructionMockDataModel();
            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.GetAll()).ReturnsAsync(response);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);
            var instructionList = await instructionMasterController.GetAll() as JsonResult;
            Assert.IsNotNull(instructionList);
            Assert.AreEqual(response.Data, ((GeneralResponse<ICollection<InstructionMaster>>)instructionList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((GeneralResponse<ICollection<InstructionMaster>>)instructionList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((GeneralResponse<ICollection<InstructionMaster>>)instructionList.Value).Result);
        }

        [Test]
        public async Task Test_GetSearchList_OkResult()
        {
            var response = GetInstructionMockDataModel();
            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.GetPageWiseData(searchFilter)).ReturnsAsync(response);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);
            var instructionMasterList = await instructionMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNotNull(instructionMasterList);
            Assert.AreEqual(response.Data, ((PagedResult<InstructionMaster>)instructionMasterList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((PagedResult<InstructionMaster>)instructionMasterList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((PagedResult<InstructionMaster>)instructionMasterList.Value).Result);
        }

        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            instructionMasterController = new InstructionMasterController(localizer.Object, instructionMasterService.Object);
            var instructionList = await instructionMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(instructionList);
            //Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)instructionList.StatusCode);
        }

        [Test]
        public async Task Test_GetAsync_With_ValidID()
        {
            GeneralResponse<InstructionMaster> obj = new GeneralResponse<InstructionMaster>()
            {
                Data = GetMockObject(2)
            };

            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.Get(obj.Data.InstructionMasterID)).ReturnsAsync(obj);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);

            var lst = await instructionMasterController.GetInstruction(obj.Data.InstructionMasterID) as OkObjectResult;
            var actualResult = lst.Value;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(obj.Data.DescriptionEn, ((GeneralResponse<InstructionMaster>)actualResult).Data.DescriptionEn);
            Assert.AreEqual(obj.Data.DescriptionFr, ((GeneralResponse<InstructionMaster>)actualResult).Data.DescriptionFr);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)lst.StatusCode);
        }

        [Test]
        public async Task Test_GetAsync_With_InValidID()
        {
            GeneralResponse<InstructionMaster> objInstruction = new GeneralResponse<InstructionMaster>()
            {
                Data = GetMockObject(99999)
            };

            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.Get(objInstruction.Data.InstructionMasterID)).ReturnsAsync(objInstruction);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);

            var instructionObject = await instructionMasterController.GetInstruction(objInstruction.Data.InstructionMasterID) as NotFoundResult;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(instructionObject);
        }

        [Test]
        public async Task Test_CreateAsync()
        {
            GeneralResponse<InstructionMaster> objInstruction1 = new GeneralResponse<InstructionMaster>()
            {
                Data = GetMockObject(1)
            };
            GeneralResponse<InstructionMasterModel> objInstruction2 = new GeneralResponse<InstructionMasterModel>()
            {
                Data = new InstructionMasterModel()
                {
                    InstructionCategoryID = 1,
                    SiteID = 1,
                    Color = "yellow",
                    DescriptionEn = "desc en",
                    DescriptionFr = "desc fr",
                    InstructionGroupID = 1,
                    GroupDisplayOrder = 1,
                    GroupItemDisplayOrder = 1
                }
            };

            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.Create(objInstruction2.Data)).ReturnsAsync(objInstruction1);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);

            var instructionObject = await instructionMasterController.PostInstruction(objInstruction2.Data);
            Assert.That(instructionObject, Is.TypeOf<ActionResult<InstructionMaster>>()); //return true
            Assert.IsNotNull(instructionObject);
        }

        [Test]
        public async Task Test_UpdateAsync()
        {
            int instructionId = 1;
            GeneralResponse<InstructionMaster> objInstruction1 = new GeneralResponse<InstructionMaster>()
            {
                Data = GetMockObject(1)
            };
            GeneralResponse<InstructionMasterModel> objInstruction2 = new GeneralResponse<InstructionMasterModel>()
            {
                Data = new InstructionMasterModel()
                {
                    InstructionCategoryID = 1,
                    SiteID = 1,
                    Color = "yellow",
                    DescriptionEn = "desc en",
                    DescriptionFr = "desc fr",
                    InstructionGroupID = 1,
                    GroupDisplayOrder = 1,
                    GroupItemDisplayOrder = 1
                }
            };
            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.Update(objInstruction2.Data)).ReturnsAsync(objInstruction1);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);

            var instructionObject = await instructionMasterController.PutInstruction(instructionId, objInstruction2.Data) as JsonResult;

            Assert.That(instructionObject.Value, Is.EqualTo(objInstruction1));
            Assert.IsNotNull(instructionObject);
        }

        [Test]
        public async Task Test_DeleteAsync()
        {
            int recordID = 1;
            var response = new GeneralResponse<bool>();

            mydata = new Mock<IInstructionMasterService>();
            mydata.Setup(t => t.Delete(recordID)).ReturnsAsync(response);

            instructionMasterController = new InstructionMasterController(localizer.Object, mydata.Object);

            var instructionObject = await instructionMasterController.DeleteInstruction(recordID) as JsonResult;

            Assert.That(instructionObject.Value, Is.EqualTo(response));
            Assert.IsNotNull(instructionObject);
        }

        private GeneralResponse<ICollection<InstructionMaster>> GetGeneralinstructionMockData()
        {
            var response = new GeneralResponse<ICollection<InstructionMaster>>();
            List<InstructionMaster> instructionPermissionMatrix = new List<InstructionMaster>();
            instructionPermissionMatrix.Add(GetMockObject(99999));
            instructionPermissionMatrix.Add(GetMockObject(44444));
            response.Data = instructionPermissionMatrix;

            return response;
        }

        private PagedResult<InstructionMaster> GetInstructionMockData()
        {
            var response = new PagedResult<InstructionMaster>();
            List<InstructionMaster> instructionMasterObj = new List<InstructionMaster>();
            instructionMasterObj.Add(GetMockObject(99999));
            instructionMasterObj.Add(GetMockObject(44444));
            response.Data = instructionMasterObj;
            response.CurrentPage = 1;
            response.PageCount = 1;
            response.PageSize = 10;
            response.RowCount = 2;
            response.Message = null;
            response.Exception = null;
            response.Result = ResultType.Success;
            response.ExtraData = null;
            return response;
        }

        private InstructionMaster GetMockObject(int? id = null)
        {
            var obj = new InstructionMaster()
            {
                InstructionMasterID = 99999,
                InstructionCategoryID = 1,
                SiteID = 1,
                Color = "yellow",
                DescriptionEn = "desc en",
                DescriptionFr = "desc fr",
                InstructionGroupID = 1,
                GroupDisplayOrder = 1,
                GroupItemDisplayOrder = 1
            };
            if(id != null)
            {
                obj.InstructionMasterID = Convert.ToInt32(id);
            }
            return obj;
        }

        private GeneralResponse<ICollection<InstructionMasterModel>> GetGeneralinstructionMockDataModel()
        {
            var response = new GeneralResponse<ICollection<InstructionMasterModel>>();
            List<InstructionMasterModel> instructionPermissionMatrix = new List<InstructionMasterModel>();
            instructionPermissionMatrix.Add(GetMockObjectModel(99999));
            instructionPermissionMatrix.Add(GetMockObjectModel(44444));
            response.Data = instructionPermissionMatrix;

            return response;
        }

        private PagedResult<InstructionMasterModel> GetInstructionMockDataModel()
        {
            var response = new PagedResult<InstructionMasterModel>();
            List<InstructionMasterModel> instructionMasterObj = new List<InstructionMasterModel>();
            instructionMasterObj.Add(GetMockObjectModel(99999));
            instructionMasterObj.Add(GetMockObjectModel(44444));
            response.Data = instructionMasterObj;
            response.CurrentPage = 1;
            response.PageCount = 1;
            response.PageSize = 10;
            response.RowCount = 2;
            response.Message = null;
            response.Exception = null;
            response.Result = ResultType.Success;
            response.ExtraData = null;
            return response;
        }

        private InstructionMasterModel GetMockObjectModel(int? id = null)
        {
            var obj = new InstructionMasterModel()
            {
                InstructionMasterID = 99999,
                InstructionCategoryID = 1,
                SiteID = 1,
                Color = "yellow",
                DescriptionEn = "desc en",
                DescriptionFr = "desc fr",
                InstructionGroupID = 1,
                GroupDisplayOrder = 1,
                GroupItemDisplayOrder = 1
            };
            if (id != null)
            {
                obj.InstructionMasterID = Convert.ToInt32(id);
            }
            return obj;
        }
    }
}