﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
using FluentValidation;
using FluentValidation.TestHelper;
using System.Globalization;

namespace FM21.Tests
{
    public class RegulatoryMasterTest
    {
        private Mock<IRegulatoryMasterService> regulatoryService;

        private RegulatoryMasterController regulatoryController;
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        Mock<IRegulatoryMasterService> mydata;
        private RegulatoryMasterValidator validator;

        [SetUp]
        [SetUICulture("en-us")]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            regulatoryService = new Mock<IRegulatoryMasterService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                     m.PageSize == 10 &&
                     m.PageIndex == 1 &&
                     m.Search == "FM" &&
                     m.SortColumn == "" &&
                     m.SortDirection == "");
           // localizer.Setup(x => x.WithCulture(CultureInfo.CurrentCulture));
            validator = new RegulatoryMasterValidator();
            
        }

        [Test]
        public async Task Test_Get_AllRegulatory()
        {

            var response = getGeneralRegulatoryMockData();
            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.GetAll()).ReturnsAsync(response);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);
            var regulatoryList = await regulatoryController.GetAll() as JsonResult;
            Assert.IsNotNull(regulatoryList);
            Assert.AreEqual(response.Data, ((GeneralResponse<ICollection<RegulatoryMaster>>)regulatoryList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((GeneralResponse<ICollection<RegulatoryMaster>>)regulatoryList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((GeneralResponse<ICollection<RegulatoryMaster>>)regulatoryList.Value).Result);
        }
        [Test]
        public async Task Test_GetSearchList_OkResult()
        {

            var response = getRegulatoryMockData();
            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.GetPageWiseData(searchFilter.Search, searchFilter.PageIndex, searchFilter.PageSize, searchFilter.SortColumn, searchFilter.SortDirection)).ReturnsAsync(response);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);
            var regulatoryList = await regulatoryController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNotNull(regulatoryList);
            Assert.AreEqual(response.Data, ((PagedResult<RegulatoryModel>)regulatoryList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((PagedResult<RegulatoryModel>)regulatoryList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((PagedResult<RegulatoryModel>)regulatoryList.Value).Result);

        }
        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            regulatoryController = new RegulatoryMasterController(localizer.Object, regulatoryService.Object);
            var regulatoryList = await regulatoryController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(regulatoryList.Value);

        }

        [Test]
        public async Task Test_GetregulatoryAsync_With_ValidRegulatoryId()
        {
            GeneralResponse<RegulatoryMaster> objRegulatory = new GeneralResponse<RegulatoryMaster>()
            {
                Data = new RegulatoryMaster()
                {

                    RegulatoryId = 1,
                    NutrientId = 1,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };

            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.Get(objRegulatory.Data.RegulatoryId)).ReturnsAsync(objRegulatory);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);

            var regulatoryList = await regulatoryController.GetRegulatory(objRegulatory.Data.RegulatoryId) as JsonResult;
            var actualResult = regulatoryList.Value;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(objRegulatory.Data.NutrientId, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.NutrientId);
            Assert.AreEqual(objRegulatory.Data.OldUsa, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.OldUsa);
            Assert.AreEqual(objRegulatory.Data.CanadaNi, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.CanadaNi);
            Assert.AreEqual(objRegulatory.Data.CanadaNf, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.CanadaNf);
            Assert.AreEqual(objRegulatory.Data.NewUsRdi, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.NewUsRdi);
            Assert.AreEqual(objRegulatory.Data.EU, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.EU);
            Assert.AreEqual(objRegulatory.Data.Unit, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.Unit);
            Assert.AreEqual(objRegulatory.Data.UnitPerMg, ((GeneralResponse<RegulatoryMaster>)actualResult).Data.UnitPerMg);

            //Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)regulatoryList.StatusCode);

        }

        [Test]
        public async Task Test_GetRegulatoryAsync_With_InValidRegulatoryId()
        {
            int regulatoryId = 100;
            GeneralResponse<RegulatoryMaster> objRegulatory = new GeneralResponse<RegulatoryMaster>()
            {
                Data = new RegulatoryMaster()
                {
                    RegulatoryId = 50,
                    NutrientId = 1,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };

            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.Get(objRegulatory.Data.RegulatoryId)).ReturnsAsync(objRegulatory);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);

            var regulatoryObject = await regulatoryController.GetRegulatory(regulatoryId) as NotFoundResult;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(HttpStatusCode.NotFound, (HttpStatusCode)regulatoryObject.StatusCode);

        }

        [Test]
        public async Task Test_CreateRegulatoryAsync()
        {
            GeneralResponse<RegulatoryMaster> objRegulatory = new GeneralResponse<RegulatoryMaster>()
            {
                Data = new RegulatoryMaster()
                {

                    RegulatoryId = 1,
                    NutrientId = 1,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                    CreatedBy = 25412,
                    CreatedOn = DateTime.Now,
                }
            };
            GeneralResponse<RegulatoryModel> objRegulatory1 = new GeneralResponse<RegulatoryModel>()
            {
                Data = new RegulatoryModel()
                {

                    RegulatoryId = 0,
                    NutrientId = 1,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };
           
          

            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.Create(objRegulatory1.Data)).ReturnsAsync(objRegulatory);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);

            var regulatoryObject = await regulatoryController.PostRegulatory(objRegulatory1.Data);
            Assert.That(regulatoryObject, Is.TypeOf<ActionResult<RegulatoryMaster>>()); //return true
            Assert.IsNotNull(regulatoryObject);
        }

        [Test]
        public async Task Test_UpdateRegulatoryAsync()
        {
            int RegulatoryId = 1;
            GeneralResponse<RegulatoryMaster> objRegulatory = new GeneralResponse<RegulatoryMaster>()
            {
                Data = new RegulatoryMaster()
                {
                    RegulatoryId = 1,
                    NutrientId = 1,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };
            GeneralResponse<RegulatoryModel> objRegulatory1 = new GeneralResponse<RegulatoryModel>()
            {
                Data = new RegulatoryModel()
                {
                    RegulatoryId = 1,
                    NutrientId = 2,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };
            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.Update(objRegulatory1.Data)).ReturnsAsync(objRegulatory);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);

            var regulatoryObject = await regulatoryController.PutRegulatory(RegulatoryId, objRegulatory1.Data) as JsonResult;

            Assert.That(
            regulatoryObject.Value,
            Is.EqualTo(objRegulatory));
            Assert.IsNotNull(regulatoryObject);
        }

        [Test]
        [SetUICulture("en-us")]
        public async Task Test_UpdateRegulatoryAsync_WithInvalidData()
        {
           
            GeneralResponse<RegulatoryModel> objRegulatory = new GeneralResponse<RegulatoryModel>()
            {
                Data = new RegulatoryModel()
                {
                    RegulatoryId =0,
                    NutrientId = 0,
                    OldUsa = 1000,
                    CanadaNi = 5,
                    CanadaNf = 100,
                    NewUsRdi = 5,
                    EU = 500,
                    Unit = "mg",
                    UnitPerMg = 100,
                    IsActive = true,
                    IsDeleted = false,
                }
            };
         
            var result =  validator.TestValidate(objRegulatory.Data);
            result.ShouldHaveValidationErrorFor(x => x.NutrientId);
            result.ShouldHaveValidationErrorFor(x => x.RegulatoryId);
           
        }

        [Test]
        public async Task Test_DeleteRegulatoryAsync()
        {
            int RegulatoryId = 1;
            var response = new GeneralResponse<bool>();

            mydata = new Mock<IRegulatoryMasterService>();
            mydata.Setup(t => t.Delete(RegulatoryId)).ReturnsAsync(response);

            regulatoryController = new RegulatoryMasterController(localizer.Object, mydata.Object);

            var regulatoryObject = await regulatoryController.DeleteRegulatory(RegulatoryId) as JsonResult;

            Assert.That(
            regulatoryObject.Value,
            Is.EqualTo(response));
            Assert.IsNotNull(regulatoryObject);
        }

        private PagedResult<RegulatoryModel> getRegulatoryMockData()
        {
            var response = new PagedResult<RegulatoryModel>();
            List<RegulatoryModel> regulatoryMaster = new List<RegulatoryModel>();
            regulatoryMaster.Add(new RegulatoryModel()
            {
                RegulatoryId = 1,
                NutrientId = 2,
                OldUsa = 1000,
                CanadaNi = 5,
                CanadaNf = 100,
                NewUsRdi = 5,
                EU = 500,
                Unit = "mg",
                UnitPerMg = 100,
                IsActive = true,
                IsDeleted = false
              

            });
            regulatoryMaster.Add(new RegulatoryModel()
            {
                RegulatoryId = 2,
                NutrientId =1,
                OldUsa = 1000,
                CanadaNi = 5,
                CanadaNf = 100,
                NewUsRdi = 5,
                EU = 500,
                Unit = "mg",
                UnitPerMg = 50,
                IsActive = true,
                IsDeleted = false
             
            });
            response.Data = regulatoryMaster;
            response.CurrentPage = 1;
            response.PageCount = 1;
            response.PageSize = 10;
            response.RowCount = 2;
            response.Message = null;
            response.Exception = null;
            response.Result = ResultType.Success;
            response.ExtraData = null;

            return response;
        }
        private GeneralResponse<ICollection<RegulatoryMaster>> getGeneralRegulatoryMockData()
        {
            var response = new GeneralResponse<ICollection<RegulatoryMaster>>();
            List<RegulatoryMaster> regulatoryMaster = new List<RegulatoryMaster>();
            regulatoryMaster.Add(new RegulatoryMaster()
            {
                RegulatoryId = 1,
                NutrientId = 2,
                OldUsa = 1000,
                CanadaNi = 5,
                CanadaNf = 100,
                NewUsRdi = 5,
                EU = 500,
                Unit = "mg",
                UnitPerMg = 100,
                IsActive = true,
                IsDeleted = false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,

            });
            regulatoryMaster.Add(new RegulatoryMaster()
            {
                RegulatoryId = 2,
                NutrientId = 1,
                OldUsa = 1000,
                CanadaNi = 5,
                CanadaNf = 100,
                NewUsRdi = 5,
                EU = 500,
                Unit = "mg",
                UnitPerMg = 50,
                IsActive = true,
                IsDeleted = false,
                CreatedBy = 25412,
                CreatedOn = DateTime.Now,
                UpdatedBy = null,
                UpdatedOn = DateTime.Now,

            });
            response.Data = regulatoryMaster;

            return response;
        }
    }

    public class RegulatoryMasterValidator : AbstractValidator<RegulatoryModel>
    {
        public RegulatoryMasterValidator()
        {
            RuleFor(x => x.RegulatoryId).
                 NotNull().WithMessage("Could not be zero.")
                 .GreaterThan(0).WithMessage("Must be greater than zero.");

            RuleFor(x => x.NutrientId)
           .NotNull().WithMessage("Could not be zero.")
           .GreaterThan(0).WithMessage("Must be greater than zero.");


            RuleFor(x => x.Unit)
            .NotEmpty().WithMessage("Could not be null or empty.");
            RuleFor(x => x.UnitPerMg)
            .NotEmpty().WithMessage("Could not be null or empty.");
        }
    }


}


