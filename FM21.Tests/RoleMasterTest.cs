﻿using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace FM21.Tests
{
    public class RoleMasterTest
    {
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        private Mock<IRoleMasterService> roleMasterService;
        private RoleMasterController roleMasterController;
        Mock<IRoleMasterService> mydata;

        [SetUp]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            roleMasterService = new Mock<IRoleMasterService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                 m.PageSize == 10 &&
                 m.PageIndex == 1 &&
                 m.Search == "test" &&
                 m.SortColumn == "" &&
                 m.SortDirection == "");
        }

        [Test]
        public async Task Test_Get_All()
        {
           
            var response = getGeneralroleMockData();
            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.GetAll()).ReturnsAsync(response);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);
            var customerList = await roleMasterController.GetAll() as JsonResult;
            Assert.IsNotNull(customerList);
            Assert.AreEqual(response.Data, ((GeneralResponse<ICollection<RoleMaster>>)customerList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((GeneralResponse<ICollection<RoleMaster>>)customerList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((GeneralResponse<ICollection<RoleMaster>>)customerList.Value).Result);
        }

        [Test]
        public async Task Test_GetSearchList_OkResult()
        {
           
            var response = getRoleMockData();
            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.GetPageWiseData(searchFilter)).ReturnsAsync(response);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);
            var roleMasterList = await roleMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNotNull(roleMasterList);
            Assert.AreEqual(response.Data, ((PagedResult<RoleMaster>)roleMasterList.Value).Data);
            Assert.AreEqual(response.Data.Count, ((PagedResult<RoleMaster>)roleMasterList.Value).Data.Count);
            Assert.AreEqual(response.Result, ((PagedResult<RoleMaster>)roleMasterList.Value).Result);
        }

        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            roleMasterController = new RoleMasterController(localizer.Object, roleMasterService.Object);
            var roleList = await roleMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(roleList);
            //Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)roleList.StatusCode);
        }

        [Test]
        public async Task Test_GetAsync_With_ValidID()
        {
            GeneralResponse<RoleMaster> obj = new GeneralResponse<RoleMaster>()
            {
                Data = new RoleMaster()
                {
                    RoleID = 2,
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };

            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.Get(obj.Data.RoleID)).ReturnsAsync(obj);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);

            var lst = await roleMasterController.GetRole(obj.Data.RoleID) as OkObjectResult;
            var actualResult = lst.Value;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(obj.Data.RoleName, ((GeneralResponse<RoleMaster>)actualResult).Data.RoleName);
            Assert.AreEqual(obj.Data.RoleDescription, ((GeneralResponse<RoleMaster>)actualResult).Data.RoleDescription);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)lst.StatusCode);
        }

        [Test]
        public async Task Test_GetAsync_With_InValidID()
        {
            GeneralResponse<RoleMaster> objRole = new GeneralResponse<RoleMaster>()
            {
                Data = new RoleMaster()
                {
                    RoleID = 99999,
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };

            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.Get(objRole.Data.RoleID)).ReturnsAsync(objRole);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);

            var roleObject = await roleMasterController.GetRole(objRole.Data.RoleID) as NotFoundResult;

            mydata.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(roleObject);
        }

        [Test]
        public async Task Test_CreateAsync()
        {
            GeneralResponse<RoleMaster> objRole1 = new GeneralResponse<RoleMaster>()
            {
                Data = new RoleMaster()
                {
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };
            GeneralResponse<RoleMasterModel> objRole2 = new GeneralResponse<RoleMasterModel>()
            {
                Data = new RoleMasterModel()
                {
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };


            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.Create(objRole2.Data)).ReturnsAsync(objRole1);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);

            var roleObject = await roleMasterController.PostRole(objRole2.Data);
            Assert.That(roleObject, Is.TypeOf<ActionResult<RoleMaster>>()); //return true
            Assert.IsNotNull(roleObject);
        }

        [Test]
        public async Task Test_UpdateAsync()
        {
            int roleId = 1;
            GeneralResponse<RoleMaster> objRole1 = new GeneralResponse<RoleMaster>()
            {
                Data = new RoleMaster()
                {
                    RoleID = 1,
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };
            GeneralResponse<RoleMasterModel> objRole2 = new GeneralResponse<RoleMasterModel>()
            {
                Data = new RoleMasterModel()
                {
                    RoleName = "test role",
                    RoleDescription = "desc"
                }
            };
            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.Update(objRole2.Data)).ReturnsAsync(objRole1);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);

            var roleObject = await roleMasterController.PutRole(roleId, objRole2.Data) as JsonResult;

            Assert.That(roleObject.Value, Is.EqualTo(objRole1));
            Assert.IsNotNull(roleObject);
        }

        [Test]
        public async Task Test_DeleteAsync()
        {
            int recordID = 1;
            var response = new GeneralResponse<bool>();

            mydata = new Mock<IRoleMasterService>();
            mydata.Setup(t => t.Delete(recordID)).ReturnsAsync(response);

            roleMasterController = new RoleMasterController(localizer.Object, mydata.Object);

            var roleObject = await roleMasterController.DeleteRole(recordID) as JsonResult;

            Assert.That(roleObject.Value, Is.EqualTo(response));
            Assert.IsNotNull(roleObject);
        }

        private GeneralResponse<ICollection<RoleMaster>> getGeneralroleMockData()
        {
            var response = new GeneralResponse<ICollection<RoleMaster>>();
            List<RoleMaster> rolePermissionMatrix = new List<RoleMaster>();
            rolePermissionMatrix.Add(new RoleMaster()
            {
                RoleID = 99999,
                RoleName = "test role1",
                RoleDescription = "desc1"
            });
            rolePermissionMatrix.Add(new RoleMaster()
            {
                RoleID = 44444,
                RoleName = "test role2",
                RoleDescription = "desc2"
            });
            response.Data = rolePermissionMatrix;

            return response;
        }

        private PagedResult<RoleMaster> getRoleMockData()
        {
            var response = new PagedResult<RoleMaster>();
            List<RoleMaster> roleMasterObj = new List<RoleMaster>();
            roleMasterObj.Add(new RoleMaster()
            {
                RoleID = 99999,
                RoleName = "test role1",
                RoleDescription = "desc1"
            });
            roleMasterObj.Add(new RoleMaster()
            {
                RoleID = 44444,
                RoleName = "test role2",
                RoleDescription = "desc2"
            });
            response.Data = roleMasterObj;
            response.CurrentPage = 1;
            response.PageCount = 1;
            response.PageSize = 10;
            response.RowCount = 2;
            response.Message = null;
            response.Exception = null;
            response.Result = ResultType.Success;
            response.ExtraData = null;

            return response;
        }
    }
}