﻿using AutoMapper;
using FM21.API.Controllers;
using FM21.Core;
using FM21.Core.Model;
using FM21.Entities;
using FM21.Service;
using FM21.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Tests
{
    public class SupplierMasterTest
    {

        private Mock<ISupplierMasterService> supplierMasterService;
        GeneralResponse<ICollection<SupplierMaster>> lstSupplier;
        private SupplierMasterController supplierMasterController;
        private Mock<IWebWorker> webWorker;
        private Mock<IStringLocalizer> localizer;
        private Mock<IMapper> mapper;
        private SearchFilter searchFilter;
        Mock<ISupplierMasterService> supplierData;

        [SetUp]
        public void SetUp()
        {
            webWorker = new Mock<IWebWorker>();
            localizer = new Mock<IStringLocalizer>();
            mapper = new Mock<IMapper>();
            supplierMasterService = new Mock<ISupplierMasterService>();
            //searchFilter = new Mock<SearchFilter>();
            searchFilter = Mock.Of<SearchFilter>(m =>
                     m.PageSize == 10 &&
                     m.PageIndex == 1 &&
                     m.Search == "a" && m.SortColumn == "SupplierName" && m.SortDirection == "asc");
        }

        [Test]
        public void Test_Get_AllSupplier()
        {

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierMasterService.Object);
            var supplierList = supplierMasterController.GetAll();
            Assert.IsNotNull(supplierList);
            Assert.That(supplierList, Is.TypeOf<System.Threading.Tasks.Task<IActionResult>>()); //Tests exact type

        }

        [Test]
        public async Task Test_GetSearchList_OkResult()
        {

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierMasterService.Object);
            var supplierList = await supplierMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNotNull(supplierList);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)supplierList.StatusCode);
        }
        [Test]
        public async Task Test_GetSearchList_BadRequestResult()
        {
            searchFilter = new SearchFilter();// This will cause modelInvalid
            supplierMasterController = new SupplierMasterController(localizer.Object, supplierMasterService.Object);
            var supplierList = await supplierMasterController.GetSearchList(searchFilter) as JsonResult;
            Assert.IsNull(supplierList);
            //Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)customerList.StatusCode);
        }

        [Test]
        public async Task Test_GetSupplierAsync_With_ValidSupplierId()
        {
            GeneralResponse<SupplierMaster> objSupplier = new GeneralResponse<SupplierMaster>()
            {
                Data = new SupplierMaster()
                {
                    SupplierID = 2,
                    SupplierName = "Santosh",
                    Address = "Ahmedabad",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };

            supplierData = new Mock<ISupplierMasterService>();
            supplierData.Setup(t => t.Get(objSupplier.Data.SupplierID)).ReturnsAsync(objSupplier);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var supplierList = await supplierMasterController.GetSupplier(objSupplier.Data.SupplierID) as OkObjectResult;
            var actualResult = supplierList.Value;

            supplierData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(objSupplier.Data.SupplierID, ((GeneralResponse<SupplierMaster>)actualResult).Data.SupplierID);
            Assert.AreEqual(objSupplier.Data.SupplierName, ((GeneralResponse<SupplierMaster>)actualResult).Data.SupplierName);
            Assert.AreEqual(objSupplier.Data.SupplierAbbreviation1, ((GeneralResponse<SupplierMaster>)actualResult).Data.SupplierAbbreviation1);
            Assert.AreEqual(objSupplier.Data.SupplierAbbreviation2, ((GeneralResponse<SupplierMaster>)actualResult).Data.SupplierAbbreviation2);
            Assert.AreEqual(objSupplier.Data.Address, ((GeneralResponse<SupplierMaster>)actualResult).Data.Address);
            Assert.AreEqual(objSupplier.Data.PhoneNumber, ((GeneralResponse<SupplierMaster>)actualResult).Data.PhoneNumber);
            Assert.AreEqual(objSupplier.Data.Email, ((GeneralResponse<SupplierMaster>)actualResult).Data.Email);
            Assert.AreEqual(HttpStatusCode.OK, (HttpStatusCode)supplierList.StatusCode);

        }

        [Test]
        public async Task Test_GetSupplierAsync_With_InValidSupplierId()
        {
            GeneralResponse<SupplierMaster> objSupplier = new GeneralResponse<SupplierMaster>()
            {
                Data = new SupplierMaster()
                {
                    SupplierID = 1212,
                    SupplierName = "Santosh",
                    Address = "Ahmedabad",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };

            supplierData = new Mock<ISupplierMasterService>();
            supplierData.Setup(t => t.Get(objSupplier.Data.SupplierID)).ReturnsAsync(objSupplier);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var customerObject = await supplierMasterController.GetSupplier(objSupplier.Data.SupplierID) as NotFoundResult;

            supplierData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(customerObject);
        }

        [Test]
        public async Task Test_CreateSupplierAsync_With_ValidData()
        {
            GeneralResponse<SupplierMaster> objSupplier1 = new GeneralResponse<SupplierMaster>()
            {
                Data = new SupplierMaster()
                {

                    SupplierName = "Santosh",
                    Address = "Ahmedabad",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };

            GeneralResponse<SupplierMasterModel> objSupplier2 = new GeneralResponse<SupplierMasterModel>()
            {
                Data = new SupplierMasterModel()
                {

                    SupplierName = "Ameet",
                    Address = "Ahmedabad",
                    PhoneNumber = "4455454",
                    Email = "a@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };


            supplierData = new Mock<ISupplierMasterService>();
            supplierData.Setup(t => t.Create(objSupplier2.Data)).ReturnsAsync(objSupplier1);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var supplierObject = await supplierMasterController.GetSupplier(objSupplier1.Data.SupplierID) as NotFoundResult;

            supplierData.Verify(c => c.Get(It.IsAny<int>()), Times.Once);
            Assert.IsNull(supplierObject);
        }

        [Test]
        public async Task Test_DeleteSupplier_OkResult()
        {
            GeneralResponse<bool> response = new GeneralResponse<bool>();
            supplierData = new Mock<ISupplierMasterService>();

            supplierData.Setup(t => t.Delete(1)).ReturnsAsync(response);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var result = await supplierMasterController.DeleteSupplier(1) as JsonResult;
            Assert.IsNotNull(result.Value);


        }

        [Test]
        public async Task Test_DeleteSupplier_NotFoundResult()
        {
            GeneralResponse<bool> response = new GeneralResponse<bool>();
            supplierData = new Mock<ISupplierMasterService>();

            supplierData.Setup(t => t.Delete(100)).ReturnsAsync(response);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var result = await supplierMasterController.DeleteSupplier(1) as JsonResult;
            Assert.IsNull(result.Value);


        }

        [Test]
        public async Task Test_UpdateSupplierAsync_With_ValidData()
        {
            int SupplierId = 1;
            GeneralResponse<SupplierMaster> objSupplier1 = new GeneralResponse<SupplierMaster>()
            {
                Data = new SupplierMaster()
                {
                    SupplierID = 1,
                    SupplierName = "Santosh",
                    Address = "Ahmedabad",
                    PhoneNumber = "12345678",
                    Email = "FM@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };

            GeneralResponse<SupplierMasterModel> objSupplier2 = new GeneralResponse<SupplierMasterModel>()
            {
                Data = new SupplierMasterModel()
                {

                    SupplierName = "Ameet",
                    Address = "Ahmedabad",
                    PhoneNumber = "4455454",
                    Email = "a@gmail.com",
                    SupplierAbbreviation1 = "This is Abbreviation 1",
                    SupplierAbbreviation2 = "This is Abbreviation 2"
                }
            };
            supplierData = new Mock<ISupplierMasterService>();
            supplierData.Setup(t => t.Update(objSupplier2.Data)).ReturnsAsync(objSupplier1);

            supplierMasterController = new SupplierMasterController(localizer.Object, supplierData.Object);

            var supplierObject = await supplierMasterController.PutSupplier(SupplierId, objSupplier2.Data) as JsonResult;

            Assert.That(
            supplierObject.Value,
            Is.EqualTo(objSupplier1));
            Assert.IsNotNull(supplierObject);
        }

    }
}
