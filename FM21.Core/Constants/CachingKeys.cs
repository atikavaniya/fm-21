﻿namespace FM21.Core
{
    public static class CachingKeys
    {
        public static string User = "User";
        public static string InstructionMaster = "InstructionMaster";
        public static string SupplierMaster = "SupplierMaster";
    }
}