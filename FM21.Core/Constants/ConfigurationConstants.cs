﻿namespace FM21.Core
{
    public static class ConfigurationConstants
    {
        public static string DbConnectionString;
        public static bool EnableLog;
        public static string LogFilePath;
        public static string SupportedLocalization;
        public static int CacheDurationInSecond;
    }
}