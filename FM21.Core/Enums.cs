﻿using System.ComponentModel;

namespace FM21.Core
{
    public enum ItemOrigin
    {
        Header = 0,
        Cookies = 1,
        Session = 2
    }

    public enum ResultType
    {
        Success = 0,
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public enum RoleMasterEnum
    {
        Admin = 1,
    }

    public enum PermissionMasterEnum
    {
        [Description("Manage formula instruction list")]
        ManageFormulaInstruction = 2,
        [Description("Manage customer name list")]
        ManageCustomer = 3,
        [Description("Manage supplier name list")]
        ManageSupplier = 4,
        [Description("Edit Formula Ingredients")]
        EditFormulaIngredients = 5,
        [Description("Edit Formula Sub Designators/Instructions/Ingredient Order")]
        EditFormulaSubDesignatorsOrIngredient = 6,
        [Description("Edit formula attributes")]
        EditFormulaAttributes = 7,
        [Description("Delete/Archive a formula")]
        DeleteOrArchiveFormula = 8,
        [Description("Edit Ingredient data")]
        EditIngredient = 9,
        [Description("Delete/Archive an Ingredient")]
        DeleteOrArchiveIngredient = 10,
        [Description("Edit Food Safety data")]
        EditFoodSafetyData = 11,
        [Description("Print Batch Sheets: Production/Lab")]
        PrintBatchSheetsProductionOrLab = 12,
        [Description("View/Print formula reports")]
        ViewPrintformulareports = 13,
        [Description("View/Print ingredient reports")]
        ViewPrintingredientreports = 14
    }
}