﻿using AutoMapper;
using FM21.Core.Model;
using FM21.Entities;
using Microsoft.CodeAnalysis;

namespace FM21.Core
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Entity To Model
            CreateMap<RoleMaster, RoleMasterModel>();
            CreateMap<InstructionMaster, InstructionMasterModel>()
                .ForMember(d => d.Site, opt => opt.MapFrom(s => s.SiteMaster.SiteDescription))
                .ForMember(d => d.InstructionCategory, opt => opt.MapFrom(s => s.InstructionCategory.InstructionCategory))
                .ForMember(d => d.InstructionGroup, opt => opt.MapFrom(s => s.InstructionGroup.InstructionGroupName));
            CreateMap<User, UserModel>();
            CreateMap<Customer, CustomerModel>();
            CreateMap<PermissionMaster, PermissionMasterModel>();
            CreateMap<SupplierMaster, SupplierMasterModel>();
            CreateMap<RegulatoryMaster, RegulatoryModel>();
            CreateMap<AllergenMaster, AllergenMasterModel>();
            #endregion

            #region Model to Entity
            CreateMap<RoleMasterModel, RoleMaster>();
            CreateMap<InstructionMasterModel, InstructionMaster>()
                .ForMember(dest => dest.GroupDisplayOrder, act => act.Ignore())
                .ForMember(dest => dest.GroupItemDisplayOrder, act => act.Ignore());
            CreateMap<UserModel, User>();
            CreateMap<CustomerModel, Customer>()
                .ForMember(dest => dest.CustomerAbbreviation1, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.CustomerAbbreviation1) ? null : src.CustomerAbbreviation1))
                .ForMember(dest => dest.CustomerAbbreviation2, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.CustomerAbbreviation2) ? null : src.CustomerAbbreviation2));
            CreateMap<PermissionMasterModel, PermissionMaster>();
            CreateMap<RolePermissionAccess, RolePermissionMapping>().ForMember(
                    dest => dest.PermissionType,
                    opt => opt.MapFrom(src => src.isAccess ? 1 : 0));
            CreateMap<SupplierMasterModel, SupplierMaster>();
            
            CreateMap<RegulatoryModel, RegulatoryMaster>();
            CreateMap<AllergenMasterModel, AllergenMaster>();
            #endregion
        }
    }
}