﻿using Serilog;
using System;

namespace FM21.Core
{
    public class ExceptionHandler : IExceptionHandler
    {
        public void LogInformation(string Message)
        {
            Log.Information("[Information] Message :" + Message);
        }

        public void LogError(Exception ex)
        {
            Log.Fatal("[Error] Message :" + ex.Message + " | Inner Exception :" + ex.InnerException);
        }

        public void LogWarning(string Message)
        {
            Log.Warning("[Warning] Message :" + Message);
        }
    }
}