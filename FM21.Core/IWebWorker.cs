﻿using System.Collections.Generic;

namespace FM21.Core
{
    public interface IWebWorker
    {
        string RequestId { get; }
        List<HttpItem> Header { get; }
        HttpItemCollection Session { get; }
        HttpItemCollection Cookies { get; }
        HttpItemCollection Application { get; }
        HttpItemCollection FormData { get; }
        string AcceptedLanguage { get; }
        string DefaultLanguages();
        string GetRequestLanguage();
    }
}