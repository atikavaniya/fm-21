﻿namespace FM21.Core
{
    public abstract class PagedResultBase : ResponseBase
    {
        public PagedResultBase()
            : base(ResultType.Success)
        {

        }

        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
    }
}