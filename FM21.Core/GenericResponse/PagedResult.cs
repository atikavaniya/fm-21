﻿using System.Collections.Generic;

namespace FM21.Core
{
    public class PagedResult<T> : PagedResultBase where T : class
    {
        public IList<T> Data { get; set; }

        public PagedResult()
        {
            Data = new List<T>();
        }
    }
}