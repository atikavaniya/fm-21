﻿using FluentValidation;
using FM21.Core.Model;
using Microsoft.Extensions.Localization;

namespace FM21.Core.Validator
{
    public class UserValidator : AbstractValidator<UserModel>
    {
        public UserValidator(IStringLocalizer localizer)
        {
            RuleSet("all", () =>
            {
                RuleFor(x => x.Id).Must(CheckId).WithMessage(localizer["msgIDMustBeGreaterThenZero"].Value);

                RuleFor(x => x.Name).NotEmpty().WithMessage(localizer["msgNameCouldNotBENullOrEmpty"].Value);

            });

            RuleSet("id", () =>
            {
                RuleFor(x => x.Id).NotNull().WithMessage(localizer["msgIDCouldNotZero"].Value)
                         .GreaterThan(0).WithMessage(localizer["msgIDMustBeGreterThenZero"].Value);
            });
        }

        private bool CheckId(int? id)
        {
            return !id.HasValue || id.Value > 0;
        }
    }
}