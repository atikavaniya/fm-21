﻿using FluentValidation;
using FM21.Core.Model;
using Microsoft.Extensions.Localization;

namespace FM21.Core.Validator
{

    public class PermissionMasterValidator : AbstractValidator<PermissionMasterModel>
    {
        public PermissionMasterValidator(IStringLocalizer localizer)
        {
            RuleSet("all", () =>
            {
                RuleFor(x => x.PermissionFor).NotEmpty().WithMessage(localizer["msgPermissionForCouldNotBENullOrEmpty"].Value);
            });
        }
    }
}