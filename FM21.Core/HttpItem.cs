﻿namespace FM21.Core
{
    public class HttpItem
    {
        public HttpItem() { }
        public ItemOrigin Origin { get; set; }
        public string Key { get; set; }
        public object Value { get; set; }
    }
}