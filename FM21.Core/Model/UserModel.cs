﻿using FM21.Entities;

namespace FM21.Core.Model
{
    public class UserModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }

    }
}