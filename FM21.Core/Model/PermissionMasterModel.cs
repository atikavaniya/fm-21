﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FM21.Core.Model
{
   public class PermissionMasterModel
    {
        public int PermissionID { get; set; }
        public string PermissionFor { get; set; }
        public short Status { get; set; }
    }
}
