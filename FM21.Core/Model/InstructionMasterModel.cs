﻿using AutoMapper.Configuration.Annotations;

namespace FM21.Core.Model
{
    public class InstructionMasterModel
    {
        public int InstructionMasterID { get; set; }
        public int SiteID { get; set; }
        [Ignore]
        public string Site { get; set; }
        public int InstructionCategoryID { get; set; }
        [Ignore]
        public string InstructionCategory { get; set; }
        public int InstructionGroupID { get; set; }
        [Ignore]
        public string InstructionGroup { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionFr { get; set; }
        public string DescriptionEs { get; set; }
        public string Color { get; set; }
        [Ignore]
        public int GroupDisplayOrder { get; set; }
        [Ignore]
        public int GroupItemDisplayOrder { get; set; }
    }
}