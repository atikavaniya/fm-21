﻿using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FM21.Core.Model
{
    public class RolePermissionModel
    {
        public int RolePermissionID { get; set; }
        public int RoleID { get; set; }
        public int PermissionID { get; set; }
        public byte PermissionType { get; set; }

        public virtual RoleMaster RoleMaster { get; set; }
        public virtual ICollection<PermissionMaster> PermissionMaster { get; set; }
    }
}