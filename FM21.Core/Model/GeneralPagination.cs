﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FM21.Core.Model
{
   
    public class GeneralPageFilters<T> where T : class
    {
        public GeneralPageFilters()
        {
            Data = new List<T>();
        }
        public List<T> Data { get; set; }
        public int TotalCount { get; set; }

    }

    public class PagedRequest
    {
        public Filter Filter { get; set; }
        public PaginationRequest PaginationRequest { get; set; }

    }
    public class Filter
    {

        public string SearchText { get; set; }
    }

    public class PaginationRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public Sort Sort { get; set; }
        public List<SortItem> SortList { get; set; }
        public PaginationRequest()
        {
            this.SortList = new List<SortItem>();
        }
    }
    public class Sort
    {
        public string SortBy { get; set; }
        public SortDirection SortDirection { get; set; }
    }
    public enum SortDirection
    {
        Ascending,
        Descending
    }

    public class SortItem
    {
        public string SortBy { get; set; }
        public string SortDirection { get; set; } //"asc", "desc", "".
    }
}
