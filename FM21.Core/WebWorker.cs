﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace FM21.Core
{
    public class WebWorker : IWebWorker
    {
        public IConfiguration _configuration;
        public WebWorker(IActionContextAccessor actionContext, IConfiguration configuration)
        {
            _configuration = configuration;
            if (actionContext.ActionContext != null)
            {
                var httpContext = actionContext.ActionContext.HttpContext;
                if (httpContext != null && httpContext.Request.Headers.Any())
                {
                    Header = httpContext.Request.Headers.Select(header => new HttpItem { Origin = ItemOrigin.Header, Key = header.Key, Value = header.Value }).ToList();
                    if (Header.Count > 0)
                    {
                        var cultureData = Header.FirstOrDefault(key => key.Key.ToLower() == "culture");
                        if (cultureData != null)
                            AcceptedLanguage = cultureData.Value.ToString().Replace('"', ' ').Trim();
                    }
                }
                if (httpContext != null && httpContext.Request.Cookies.Any())
                    Cookies = new HttpItemCollection(httpContext.Request.Cookies
                                                               .Select(cookie => new HttpItem { Origin = ItemOrigin.Cookies, Key = cookie.Key, Value = cookie.Value }));
                RequestId = httpContext.TraceIdentifier;
            }
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(AcceptedLanguage);
        }

        public string RequestId { get; }

        public List<HttpItem> Header { get; set; }

        public HttpItemCollection Session { get; set; }

        public HttpItemCollection Cookies { get; set; }

        public HttpItemCollection Application { get; }

        public HttpItemCollection FormData { get; }

        public string AcceptedLanguage { get; set; } = "en-US";

        public string DefaultLanguage { get; set; } = "en-US";

        public string DefaultLanguages()
        {
            return DefaultLanguage;
        }

        public string GetRequestLanguage()
        {
            return AcceptedLanguage;
        }
    }
}