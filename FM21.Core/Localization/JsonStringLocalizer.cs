﻿using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace FM21.Core.Localization
{
    public class JsonStringLocalizer : IStringLocalizer
    {
        //List<JsonLocalization> localization = new List<JsonLocalization>();
        Dictionary<string, string> distResources = new Dictionary<string, string>();

        public JsonStringLocalizer()
        {
            //read all json file
            Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
            distResources = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(@"Resources/" + CultureInfo.CurrentCulture.Name + ".json"));
            // localization = JsonConvert.DeserializeObject<List<JsonLocalization>>(File.ReadAllText(@"Resources/en-US.json"));
        }
        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }
        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            //return localization.Where(l => l.LocalizedValue.Keys.Any(lv => lv == CultureInfo.CurrentCulture.Name)).Select(l => new LocalizedString(l.Key, l.LocalizedValue[CultureInfo.CurrentCulture.Name], true));
            return distResources.Select(l => new LocalizedString(l.Key, l.Value, true));
        }
        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            return new JsonStringLocalizer();
        }
        private string GetString(string name)
        {
            //var query = localization.Where(l => l.LocalizedValue.Keys.Any(lv => lv == CultureInfo.CurrentCulture.Name));
            //var value = query.FirstOrDefault(l => l.Key == name);
            var value = distResources.Where(l => l.Key == name).FirstOrDefault().Value;
            return Convert.ToString(value);
        }
    }
}