﻿using FM21.Data.Infrastructure;
using FM21.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FM21.Data.Repository
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        
    }
}
