﻿using FM21.Data.Infrastructure;
using FM21.Entities;
using System.Threading.Tasks;

namespace FM21.Data.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        
    }
}