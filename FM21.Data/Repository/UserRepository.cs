﻿using FM21.Data.Infrastructure;
using FM21.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FM21.Data.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }
    }
}