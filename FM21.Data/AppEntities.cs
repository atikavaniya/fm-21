﻿using FM21.Core;
using FM21.Data.Configuration;
using FM21.Entities;
using Microsoft.EntityFrameworkCore;

namespace FM21.Data
{
    public class AppEntities : DbContext
    {
        #region Constructor
        public AppEntities(DbContextOptions options)
        {
        }
        #endregion

        #region DB Properties 
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserMaster> UserMaster { get; set; }
        public virtual DbSet<RoleMaster> RoleMaster { get; set; }
        public virtual DbSet<PermissionMaster> PermissionMaster { get; set; }
        public virtual DbSet<RolePermissionMapping> RolePermissionMapping { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<SiteMaster> SiteMaster { get; set; }
        public virtual DbSet<InstructionCategoryMaster> InstructionCategoryMaster { get; set; }
        public virtual DbSet<InstructionSiteMapping> InstructionSiteMapping { get; set; }
        public virtual DbSet<InstructionGroupMaster> InstructionGroupMaster { get; set; }
        public virtual DbSet<InstructionMaster> InstructionMaster { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<SupplierMaster> SupplierMaster { get; set; }
        public virtual DbSet<RegulatoryMaster> RegulatoryMaster { get; set; }
        public virtual DbSet<NutrientTypeMaster> NutrientTypeMaster { get; set; }
        public virtual DbSet<NutrientMaster> NutrientMaster { get; set; }
        public virtual DbSet<AllergenMaster>  AllergenMaster { get; set; }
        #endregion

        #region Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(SecurityProvider.Decrypt(ConfigurationConstants.DbConnectionString));
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleMasterConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionMappingConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PermissionMasterConfiguration());
            modelBuilder.ApplyConfiguration(new InstructionGroupMasterConfiguration());
            modelBuilder.ApplyConfiguration(new InstructionSiteMappingConfiguration());
            modelBuilder.ApplyConfiguration(new InstructionMasterConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
            modelBuilder.ApplyConfiguration(new RegulatoryMasterConfiguration());
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}