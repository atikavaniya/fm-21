﻿using FM21.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FM21.Data.Configuration
{
    public class AllergenMasterConfiguration : IEntityTypeConfiguration<AllergenMaster>
    {
        public void Configure(EntityTypeBuilder<AllergenMaster> builder)
        {
            builder.ToTable("AllergenMaster");
            builder.HasKey(k => k.AllergenID);
            builder.Property(e => e.AllergenCode).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(e => e.AllergenName).IsRequired().HasMaxLength(100).IsUnicode(false);
            builder.Property(e => e.AllergenDescription_En).IsRequired().HasMaxLength(500).IsUnicode(false);

            builder.Property(e => e.AllergenDescription_Fr).HasMaxLength(500);

            builder.Property(e => e.AllergenDescription_Es).HasMaxLength(500);

            builder.Property(e => e.CreatedOn)
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.IsDeleted)
                .HasColumnType("bit")
                .HasDefaultValueSql("(0)");
        }
    }
}
