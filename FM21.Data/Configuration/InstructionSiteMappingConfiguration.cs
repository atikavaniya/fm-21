﻿using FM21.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FM21.Data
{
    public class InstructionSiteMappingConfiguration : IEntityTypeConfiguration<InstructionSiteMapping>
    {
        public void Configure(EntityTypeBuilder<InstructionSiteMapping> builder)
        {
            builder.ToTable("InstructionSiteMapping");
            builder.HasKey(k => k.InstructionSiteMapID);

            builder.HasOne(d => d.InstructionCategory)
                  .WithMany(p => p.InstructionSiteMapping)
                  .HasForeignKey(d => d.InstructionCategoryID)
                  .OnDelete(DeleteBehavior.ClientSetNull)
                  .HasConstraintName("FK_InstructionSiteMapInstructionCategory");

            builder.HasOne(d => d.Site)
                .WithMany(p => p.InstructionSiteMapping)
                .HasForeignKey(d => d.SiteID)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_InstructionSiteMapSiteMaster");
        }
    }
}
